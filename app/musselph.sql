-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 17, 2017 at 10:44 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `musselph`
--

-- --------------------------------------------------------

--
-- Table structure for table `area`
--

CREATE TABLE IF NOT EXISTS `area` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `banner` varchar(255) NOT NULL DEFAULT 'roxas_mussels.jpg',
  `latlng` text NOT NULL,
  `icon` varchar(30) NOT NULL DEFAULT 'icon.png',
  `content` text NOT NULL,
  `province_id` int(11) NOT NULL DEFAULT '0',
  `dissolved_oxygen` float NOT NULL,
  `salinity` float NOT NULL,
  `temperature` float NOT NULL,
  `chlorophyll_a` float NOT NULL,
  `reactive_phosphorous` float NOT NULL,
  `reactive_nitrate` float NOT NULL,
  `pom` float NOT NULL,
  `turbidity` float NOT NULL,
  `ph` float NOT NULL,
  `fecal_coliform` float NOT NULL,
  `dissolved_oxygen_mg` float NOT NULL,
  `spc` float NOT NULL,
  `tds` float NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `area`
--

INSERT INTO `area` (`id`, `name`, `banner`, `latlng`, `icon`, `content`, `province_id`, `dissolved_oxygen`, `salinity`, `temperature`, `chlorophyll_a`, `reactive_phosphorous`, `reactive_nitrate`, `pom`, `turbidity`, `ph`, `fecal_coliform`, `dissolved_oxygen_mg`, `spc`, `tds`) VALUES
(1, 'Aklan, Batan', 'roxas_mussels.jpg', '11.57655,122.48755', 'icon.png', 'Aklan, Batan', 0, 77.84, 33.644, 32.38, 0, 0, 0, 0, 0, 7.886, 0, 4.712, 51547, 33514),
(2, 'Aklan,New Washington\r\n', 'roxas_mussels.jpg', '11.591683,122.440433\r\n', 'icon.png', 'Aklan,New Washington', 0, 85.8, 30.25, 30.3, 0, 0, 0, 0, 0, 7.85, 0, 5.47, 46803, 30420),
(3, 'Guimaras,Lawi\r\n', 'roxas_mussels.jpg', '10.54335,122.532233\r\n', 'icon.png', 'Guimaras,Lawi', 0, 87.88, 32.4668, 27.25, 0, 0, 0, 0, 0, 8.09, 0, 5.89, 49706, 24852),
(4, 'Iloilo,Anilao\r\n', 'roxas_mussels.jpg', '10.9843,122.78825\r\n', 'icon.png', 'Iloilo,Anilao', 0, 90.36, 33.33, 30.76, 0, 0, 0, 0, 0, 7.95, 0, 5.62, 51036.4, 33176),
(5, 'Iloilo,Ajuy', 'roxas_mussels.jpg', '11.155783,123.042283', 'icon.png', 'Iloilo,Ajuy', 0, 86.3, 33.58, 29.2, 0, 0, 0, 0, 0, 7.89, 0, 5.49, 51315, 33345),
(6, 'Iloilo, Barotac', 'roxas_mussels.jpg', '11.0884,122.9207', 'icon.png', 'Iloilo, Barotac', 0, 76.85, 32.628, 30.13, 0, 0, 0, 0, 0, 7.921, 0, 4.845, 50054.9, 32539),
(7, 'Capiz, Sapian', 'roxas_mussels.jpg', '11.5271,122.607067', 'icon.png', 'Capiz, Sapian', 0, 66.2, 33.18, 28.7, 0, 0, 0, 0, 0, 7.85, 0, 4.25, 50744, 32955),
(8, 'Capiz, Ivisan\r\n\r\n', 'roxas_mussels.jpg', '11.599717,122.6174\r\n', 'icon.png', 'Capiz, Ivisan\r\n\r\n', 0, 64.1, 32.68, 28.6, 0, 0, 0, 0, 0, 7.73, 0, 4.15, 50052, 32565),
(9, 'Capiz, Panay', 'roxas_mussels.jpg', '11.575467,122.831933\r\n', 'icon.png', 'Capiz, Panay', 0, 99, 26.58, 29.2, 0, 0, 0, 0, 0, 7.96, 0, 6.55, 41637, 27066),
(10, 'Samar, Villareal\r\n', 'roxas_mussels.jpg', '11.603528,124.929389\r\n', 'icon.png', 'Samar, Villareal\r\n', 0, 80.5, 32.74, 30, 0, 0, 0, 0, 0, 7.85, 0, 5.09, 50195, 32630),
(11, 'Samar, Jiabong', 'roxas_mussels.jpg', '11.741667, 124.945\r\n', 'icon.png', 'Samar, Jiabong', 0, 79.6, 32.58, 30.2, 0, 0, 0, 0, 0, 7.82, 0, 5.02, 49986, 32500),
(12, 'Samar, Tarangnan\r\n', 'roxas_mussels.jpg', '11.914, 124.792972\r\n', 'icon.png', 'Samar, Tarangnan\r\n', 0, 62.9, 32.68, 29.9, 0, 0, 0, 0, 0, 7.77, 0, 3.98, 50123, 32565),
(13, 'Pangasinan,Lucao\r\n', 'roxas_mussels.jpg', '16.021889,120.314972\r\n', 'icon.png', 'Pangasinan,Lucao\r\n', 0, 43.33, 3.72, 29.71, 0, 0, 0, 0, 0, 7.67, 0, 3.23, 6840.2, 4447.63),
(14, 'Bataan, Abucay', 'roxas_mussels.jpg', '14.719833,120.567556\r\n', 'icon.png', 'Bataan, Abucay', 0, 100, 17.24, 29.2, 0, 0, 0, 0, 0, 8.06, 0, 6.95, 28153.5, 18299.2),
(15, 'Cavite, Bacoor', 'roxas_mussels.jpg', '14.485639,120.947528\r\n', 'icon.png', 'Cavite, Bacoor', 0, 48.07, 25.84, 29.53, 0, 0, 0, 0, 0, 7.66, 0, 3.19, 40507.5, 26279.5);

-- --------------------------------------------------------

--
-- Table structure for table `heat_area`
--

CREATE TABLE IF NOT EXISTS `heat_area` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `desc` text NOT NULL,
  `latlng_array` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `heat_area`
--

INSERT INTO `heat_area` (`id`, `name`, `desc`, `latlng_array`) VALUES
(6, 'TEST', 'TEST', '{lat:12.275598890561733,lng:122.10205078125,radius:15},'),
(7, 'TEST2 ', 'TEST2', '{lat:9.774024565864734,lng:118.740234375,radius:20},'),
(8, 'TEST3', 'TEST3', '{lat:9.86062814536589,lng:118.7841796875,radius:15},');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `heat_area`
--
ALTER TABLE `heat_area`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `area`
--
ALTER TABLE `area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `heat_area`
--
ALTER TABLE `heat_area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
