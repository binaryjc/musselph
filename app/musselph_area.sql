-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 16, 2017 at 03:13 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `musselph`
--

-- --------------------------------------------------------

--
-- Table structure for table `area`
--

CREATE TABLE IF NOT EXISTS `area` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `map_images_path` text NOT NULL,
  `banner` varchar(255) NOT NULL DEFAULT 'roxas_mussels.jpg',
  `latlng` text NOT NULL,
  `icon` varchar(30) NOT NULL DEFAULT 'icon.png',
  `category` varchar(350) NOT NULL,
  `content` text NOT NULL,
  `province_id` int(11) NOT NULL DEFAULT '0',
  `dissolved_oxygen` float NOT NULL,
  `salinity` float NOT NULL,
  `temperature` float NOT NULL,
  `chlorophyll_a` float NOT NULL,
  `reactive_phosphorous` float NOT NULL,
  `reactive_nitrate` float NOT NULL,
  `pom` float NOT NULL,
  `turbidity` float NOT NULL,
  `ph` float NOT NULL,
  `fecal_coliform` float NOT NULL,
  `dissolved_oxygen_mg` float NOT NULL,
  `spc` float NOT NULL,
  `tds` float NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `area`
--

INSERT INTO `area` (`id`, `name`, `map_images_path`, `banner`, `latlng`, `icon`, `category`, `content`, `province_id`, `dissolved_oxygen`, `salinity`, `temperature`, `chlorophyll_a`, `reactive_phosphorous`, `reactive_nitrate`, `pom`, `turbidity`, `ph`, `fecal_coliform`, `dissolved_oxygen_mg`, `spc`, `tds`) VALUES
(2, 'Batan', 'Batan', 'roxas_mussels.jpg', '11.527413,122.463312', 'icon.png', 'existing', '', 0, 0, 27.14, 28.93, 0, 0, 0, 0, 0, 7.74, 0, 4.46, 0, 0),
(3, 'Sapian', 'Sapian', 'roxas_mussels.jpg', '11.458060,122.685871\n', 'icon.png', 'existing', '', 0, 0, 32.12, 28.56, 0, 0, 0, 0, 0, 7.89, 0, 5.11, 0, 0),
(4, 'Pan-ay', 'Panay', 'roxas_mussels.jpg', '11.542840,123.003445\n', 'icon.png', 'existing', '', 0, 0, 20.33, 29.12, 0, 0, 0, 0, 0, 7.7, 0, 5.39, 0, 0),
(5, 'Villareal', 'Villareal', 'roxas_mussels.jpg', '11.513237,124.741001\n', 'icon.png', 'existing', '', 0, 0, 32.96, 30.04, 0, 0, 0, 0, 0, 7.84, 0, 7.98, 0, 0),
(6, 'Jiabong', 'Jiabong', 'roxas_mussels.jpg', '11.812149,125.182171', 'icon.png', 'existing', '', 0, 0, 32.6, 29.89, 0, 0, 0, 0, 0, 7.85, 0, 5.07, 0, 0),
(7, 'Tarangnan', 'Tarangnan', 'roxas_mussels.jpg', '11.835336,124.802456', 'icon.png', 'existing', '', 0, 0, 31.89, 30, 0, 0, 0, 0, 0, 7.7, 0, 3.51, 0, 0),
(8, 'Calape', 'Calape', 'roxas_mussels.jpg', '9.969770,123.908787', 'suitable.png', 'suitable', '', 0, 0, 33.83, 26.98, 0, 0, 0, 0, 0, 8.18, 0, 5.3, 0, 0),
(9, 'Hagnaya', 'Hagnaya', 'roxas_mussels.jpg', '11.158772,123.988094', 'suitable.png', 'suitable', '', 0, 0, 33.11, 28.37, 0, 0, 0, 0, 0, 8.1, 0, 5.03, 0, 0),
(10, 'Murcielagos Bay', '', 'roxas_mussels.jpg', '8.707191,123.728542', 'suitable.png', 'suitable', '', 0, 0, 33.52, 30.47, 0, 0, 0, 0, 0, 7.94, 0, 6.16, 0, 0),
(11, 'Panguil Bay', '', 'roxas_mussels.jpg', '7.978222,123.814716', 'suitable.png', 'suitable', '', 0, 0, 24.07, 30.89, 0, 0, 0, 0, 0, 7.9, 0, 5.84, 0, 0),
(12, 'Canal Bay', '', 'roxas_mussels.jpg', '9.622319,125.660419', 'suitable.png', 'suitable', '', 0, 0, 32.75, 29.18, 0, 0, 0, 0, 0, 7.97, 0, 6.24, 0, 0),
(13, 'Candijay', 'Candijay', 'roxas_mussels.jpg', '9.755659,124.615345', 'less.png', 'less', '', 0, 0, 32.91, 27.25, 0, 0, 0, 0, 0, 8.15, 0, 5.73, 0, 0),
(14, 'Talibon', 'Talibon', 'roxas_mussels.jpg', '10.097561,124.284382', 'less.png', 'less', '', 0, 0, 34, 27.08, 0, 0, 0, 0, 0, 8.22, 0, 5.62, 0, 0),
(15, 'Carmen', 'Carmen', 'roxas_mussels.jpg', '10.511347,124.062939', 'less.png', 'less', '', 0, 0, 33.88, 29, 0, 0, 0, 0, 0, 8.2, 0, 5.43, 0, 0),
(16, 'Moalboal', 'Moalboal', 'roxas_mussels.jpg', '9.910590, 123.463497', 'less.png', 'less', '', 0, 0, 33.33, 27.16, 0, 0, 0, 0, 0, 8.09, 0, 6.58, 0, 0),
(17, 'Macajalar Bay', '', 'roxas_mussels.jpg', '8.514721, 124.688473', 'less.png', 'less', '', 0, 0, 32.24, 30.55, 0, 0, 0, 0, 0, 7.98, 0, 6.22, 0, 0),
(18, 'Carrascal Bay', '', 'roxas_mussels.jpg', '9.318895, 125.997219', 'less.png', 'less', '', 0, 0, 32.39, 30.42, 0, 0, 0, 0, 0, 7.91, 0, 5.75, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `area`
--
ALTER TABLE `area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
