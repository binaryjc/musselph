<?php
class Location extends CI_Model{

	function getAllLocations(){
		$this->db->select()->from('area');
		$query = $this->db->get();
		return $query->result_array();
	}
	function getAllReds(){
		$this->db->select()->from('red_tides');
		$query = $this->db->get();
		return $query->result_array();
	}

	function addlocation($data){
		$query = $this->db->insert('area',$data);
		if($query){
			return true;
		}else{
			return false;
		}
	}
	function editLocation($data,$id){
		$where = array(
			'id'=>$id
		);
		$this->db->where($where);
		$this->db->update('area',$data);
		if($this->db->affected_rows()){
				return true;
		}
	}
	function getLocationById($id){
		$where = array('id'=>$id);
		$this->db->select()->from('area')->where($where);
		$query = $this->db->get();
		//return $query->first_row('array');
		return $query->result_array();
	}


	function delete_location($id){
		//Delete Location Data
		$this->db->delete('area', array('id' => $id));
		return true;
	}
	/*
	function getLocationByFilter($category,$search_string){
		$this->db->select()->from('locations');
		if (isset($category) && $category!="0"){
			$this->db->where('locations.category_id',$category);
		}
		if (isset($search_string) && $search_string != "0"){
			$this->db->like("locations.name",urldecode($search_string));
		}
		$query = $this->db->get();
		return $query->result_array();
	}


	function getLocationByCategory($category){
		$this->db->select()->from('locations');
		if (isset($category) && $category!="0"){
			$this->db->where('locations.category_id',$category);
		}
		$query = $this->db->get();
		return $query->result_array();
	}

	function getLocationByName($query){
		$this->db->select()->from('locations');
		//$this->db->where('locations.category_id',$category);
		$this->db->like('name', $query);
		$query = $this->db->get();
		return $query->result_array();
	}

	function getAllCategories(){
		//$this->db->select('categories.*,count(id) as total')->from('categories');
		$string_query = "SELECT categories.*,count(locations.id) as total FROM `categories` left join locations on categories.id = locations.category_id group by categories.id";
		//$query = $this->db->get();
		$query = $this->db->query($string_query);
		return $query->result_array();
	}*/

/*HEAT MAPS*/

	function getAllHeatLocations(){
		$this->db->select()->from('heat_area');
		$query = $this->db->get();
		return $query->result_array();
	}

	function addHeatLocation($data){
		$query = $this->db->insert('heat_area',$data);
		if($query){
			return true;
		}else{
			return false;
		}
	}
	function delete_heat_location($id){
		//Delete Location Data
		$this->db->delete('heat_area', array('id' => $id));
		return true;
	}

	function getHeatLocationById($id){
		$where = array('id'=>$id);
		$this->db->select()->from('heat_area')->where($where);
		$query = $this->db->get();
		//return $query->first_row('array');
		return $query->result_array();
	}
	function editHeatLocation($data,$id){
		$where = array(
			'id'=>$id
		);
		$this->db->where($where);
		$this->db->update('heat_area',$data);
		if($this->db->affected_rows()){
				return true;
		}
	}

	function getSSAMdata_byYear(){
		$where = array(
			'year'=>2015,
			'chlor_a >' => 1
		);
		$this->db->select()->from('ssam_data');
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result_array();
	}

	function getSSTdata_byYear(){
		$where = array(
			'year'=>2015,
			'sst >' => 1
		);
		$this->db->select()->from('ssam_data');
		$this->db->where($where);
		$query = $this->db->get();
		return $query->result_array();
	}

	function getDistanceBetweenPointsNew($latitude1, $longitude1, $latitude2, $longitude2, $unit = 'Mi') {
	   $theta = $longitude1 - $longitude2;
	   $distance = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2))+
	               (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta))));
	   $distance = acos($distance); $distance = rad2deg($distance);
	   $distance = $distance * 60 * 1.1515;

	   switch($unit)
	   {
	     case 'Mi': break;
	     case 'Km' : $distance = $distance * 1.609344;
	   }
	   return (round($distance,2));
	}
	/*
	SQL command for creating new column for separeate lat lng
UPDATE ssam_data SET latitude = SUBSTRING_INDEX(`latlng1`, ',', 1), longitude = SUBSTRING(SUBSTRING_INDEX(`latlng1`, ',', 2),
       LENGTH(SUBSTRING_INDEX(`latlng1`, ',', 1)) + 1);
       UPDATE ssam_data
SET longitude = replace(longitude,',','')
	*/

	//then add a query to get all the records with distance less or equal
	function getNearby100kmPoints($latitude,$longitude,$distance = 100){
		$queryString = "SELECT *
        FROM (SELECT *, (((acos(sin((".$latitude."*pi()/180)) *
        sin((`latitude`*pi()/180))+cos((".$latitude."*pi()/180)) *
        cos((`latitude`*pi()/180)) * cos(((".$longitude."-
        `longitude`)*pi()/180))))*180/pi())*60*1.1515*1.609344)
        as distance
        FROM `ssam_data`)myTable
        WHERE distance <= ".$distance."";

		$results = $this->db->query($queryString);

		$final_results = $results->result('array');
		return $final_results ;

	}

	//controller to use default current year and month (not sure how to check month available)
	function getNearby100kmPointsWithYearAndMonth($latitude,$longitude,$distance = 100,$year,$month){
		$queryString = "SELECT *
        FROM (SELECT *, (((acos(sin((".$latitude."*pi()/180)) *
        sin((`latitude`*pi()/180))+cos((".$latitude."*pi()/180)) *
        cos((`latitude`*pi()/180)) * cos(((".$longitude."-
        `longitude`)*pi()/180))))*180/pi())*60*1.1515*1.609344)
        as distance
        FROM `ssam_data` WHERE year =".$year." AND month ='".$month."' )myTable
        WHERE distance <= ".$distance." ";

		$results = $this->db->query($queryString);

		$final_results = $results->result('array');
		//print_r($final_results);

		return $final_results ;

	}
}
?>