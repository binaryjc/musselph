<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admins extends CI_Controller {

	public function index(){
		$this->load->model('location');

		$data['content'] = 'admin_dashboard';
		$this->load->view('includes/template',$data);
	}

	public function heatmap(){
		$this->load->model('location');

		$data['content'] = 'admin_dashboard_heatmap';
		$this->load->view('includes/template',$data);
	}

	public function add_location(){

		$this->load->model('location');
		if ($_POST){
			//Add
			$data = array(
				'name' => $_POST['loc_name'],
				'latlng' => $_POST['loc_latlng'],
				'content' => $_POST['loc_content'],
				'dissolved_oxygen' => $_POST['loc_oxygen'],
				'salinity' => $_POST['loc_salinity'],
				'temperature' => $_POST['loc_temperature'],
				'chlorophyll_a' => $_POST['loc_chlorophyll_a'],
				'reactive_phosphorous' => $_POST['loc_reactive_phosphorous'],
				'reactive_nitrate' => $_POST['loc_reactive_nitrate'],
				'pom' => $_POST['loc_pom'],
				'turbidity' => $_POST['loc_turbidity'],
				'ph' => $_POST['loc_ph'],
				'fecal_coliform' => $_POST['loc_fecal_coliform'],
				'dissolved_oxygen_mg' => $_POST['loc_oxygen_2'],
				'spc' => $_POST['loc_spc'],
				'tds' => $_POST['loc_tds']
			);
			$result = $this->location->addlocation($data);

					if ($result == true){
						$this->session->set_flashdata('success', 'Location was successfully added.');
						redirect("admins/");
					} else {
						$this->session->set_flashdata('errors', $result['errors']);
						redirect("admins/");
					}
		}
	}

	public function edit_location(){

		$this->load->model('location');
		if ($_POST){
			//edit data
			$data = array(
						'name' => $_POST['loc_edit_name'],
						'latlng' => $_POST['loc_edit_latlng'],
						'content' => $_POST['loc_edit_content'],
						'dissolved_oxygen' => $_POST['loc_edit_oxygen'],
						'salinity' => $_POST['loc_edit_salinity'],
						'temperature' => $_POST['loc_edit_temperature'],
						'chlorophyll_a' => $_POST['loc_edit_chlorophyll_a'],
						'reactive_phosphorous' => $_POST['loc_edit_reactive_phosphorous'],
						'reactive_nitrate' => $_POST['loc_edit_reactive_nitrate'],
						'pom' => $_POST['loc_edit_pom'],
						'turbidity' => $_POST['loc_edit_turbidity'],
						'ph' => $_POST['loc_edit_ph'],
						'fecal_coliform' => $_POST['loc_edit_fecal_coliform'],
						'dissolved_oxygen_mg' => $_POST['loc_edit_oxygen_2'],
						'spc' => $_POST['loc_edit_spc'],
						'tds' => $_POST['loc_edit_tds']
					);
			$result = $this->location->editLocation($data,$_POST['loc_selected_id']);

					if ($result == true){
						$this->session->set_flashdata('success', 'Location was successfully edited.');
						redirect("admins/");
					} else {
						$this->session->set_flashdata('errors', $result['errors']);
						redirect("admins/");
					}
		}

	}

	public function delete_location($id){
		if(isset($id)){
			$this->load->model('location');
			if($query = $this->location->delete_location($id)==true){
				$this->session->set_flashdata('success','Record(s) Successfully Deleted' );
				redirect('/admins');
			}
		}

	}

	public function add_heat_location(){

		$this->load->model('location');
		if ($_POST){
			//Add
			$data = array(
				'name' => $_POST['heat_loc_name'],
				'desc' => $_POST['heat_loc_content'],
				'latlng_array' => $_POST['heat_loc_latlng_arrays']
			);
			$result = $this->location->addHeatLocation($data);

					if ($result == true){
						$this->session->set_flashdata('success', 'Heat area was successfully added.');
						redirect("admins/heatmap");
					} else {
						$this->session->set_flashdata('errors', $result['errors']);
						redirect("admins/heatmap");
					}
		}
	}
	public function edit_heat_location(){

		$this->load->model('location');
		if ($_POST){
			//edit data
			$data = array(
						'name' => $_POST['heat_edit_name'],
						'latlng_array' => $_POST['heat_edit_latlng'],
						'desc' => $_POST['heat_edit_content'],
					);
			$result = $this->location->editHeatLocation($data,$_POST['heat_selected_id']);

					if ($result == true){
						$this->session->set_flashdata('success', 'Heat area was successfully edited.');
						redirect("admins/heatmap");
					} else {
						$this->session->set_flashdata('errors', $result['errors']);
						redirect("admins/heatmap");
					}
		}

	}
	public function delete_heat_location($id){
		if(isset($id)){
			$this->load->model('location');
			if($query = $this->location->delete_heat_location($id)==true){
				$this->session->set_flashdata('success','Record(s) Successfully Deleted' );
				redirect('/admins/heatmap');
			}
		}

	}

	function get_chlora_data(){
		$this->load->model('location');
		$ssam_data_results = $this->location->getSSAMdata_byYear();
		$geojson = array(
   			'type'      => 'FeatureCollection',
   			'features'  => array()
		);

		# Loop through rows to build feature arrays
		foreach ($ssam_data_results as $row) {
			$chlor_a = $sst = $sal = 0;
			if($row['chlor_a'] != NULL) {
				$chlor_a = floatval($row['chlor_a']);
			}
			if($row['sst'] != NULL) {
				$sst = floatval($row['sst']);
			}

		    $properties = array(
		    	"name"=> "Test".$row['id'],
        		"density"=>  $chlor_a,
        		"sst" => $sst
        	);

		    $lat1 = explode(',',$row['latlng1']);
		    $lat1_f = array(floatval($lat1[1]),floatval($lat1[0]));

		    $lat2 = explode(',',$row['latlng2']);
		    $lat2_f = array(floatval($lat2[1]),floatval($lat2[0]));

		    $lat3 = explode(',',$row['latlng3']);
		    $lat3_f = array(floatval($lat3[1]),floatval($lat3[0]));

		    $lat3 = explode(',',$row['latlng3']);
		    $lat3_f = array(floatval($lat3[1]),floatval($lat3[0]));

		    $lat4 = explode(',',$row['latlng4']);
		    $lat4_f = array(floatval($lat4[1]),floatval($lat4[0]));

        	$coords = array($lat1_f,$lat2_f,$lat4_f,$lat3_f,$lat1_f);

        	$main_coords = array($coords);
        	$geometry = array(
		    	"type"=> "Polygon",
        		"coordinates" => $main_coords
        	);

		    $feature = array(
		         'type' => 'Feature',
		         'id' => $row['id'],
		         'properties' => $properties,
		         'geometry' => $geometry
		    );

		    # Add feature arrays to feature collection array
		    array_push($geojson['features'], $feature);
		}

		//echo $this->distance(32.9697, -96.80322, 29.46786, -98.53506, "K");
		echo json_encode($geojson);

	}
	// old version
	function get_sst_data(){
		$this->load->model('location');
		//$ssam_data_results = $this->location->getSSAMdata_byYear();
		$ssam_data_results = $this->location->getNearby100kmPoints(10.71998, 122.52502,100);
		$geojson = array(
   			'type'      => 'FeatureCollection',
   			'features'  => array()
		);

		# Loop through rows to build feature arrays
		foreach ($ssam_data_results as $row) {
			$chlor_a = $sst = $sal = 0;
			if($row['chlor_a'] != NULL) {
				$chlor_a = floatval($row['chlor_a']);
			}
			if($row['sst'] != NULL) {
				$sst = floatval($row['sst']);
			}

		    $properties = array(
		    	"name"=> "Test".$row['id'],
        		"density"=>  $chlor_a,
        		"sst" => $sst
        	);

		    $lat1 = explode(',',$row['latlng1']);
		    $lat1_f = array(floatval($lat1[1]),floatval($lat1[0]));

		    $lat2 = explode(',',$row['latlng2']);
		    $lat2_f = array(floatval($lat2[1]),floatval($lat2[0]));

		    $lat3 = explode(',',$row['latlng3']);
		    $lat3_f = array(floatval($lat3[1]),floatval($lat3[0]));

		    $lat3 = explode(',',$row['latlng3']);
		    $lat3_f = array(floatval($lat3[1]),floatval($lat3[0]));

		    $lat4 = explode(',',$row['latlng4']);
		    $lat4_f = array(floatval($lat4[1]),floatval($lat4[0]));

        	$coords = array($lat1_f,$lat2_f,$lat4_f,$lat3_f,$lat1_f);

        	$main_coords = array($coords);
        	$geometry = array(
		    	"type"=> "Polygon",
        		"coordinates" => $main_coords
        	);

		    $feature = array(
		         'type' => 'Feature',
		         'id' => $row['id'],
		         'properties' => $properties,
		         'geometry' => $geometry
		    );

		    # Add feature arrays to feature collection array
		    array_push($geojson['features'], $feature);
		}

		//echo $this->distance(32.9697, -96.80322, 29.46786, -98.53506, "K");
		echo json_encode($geojson);

	}


	function get_chlora_data_with_location($current_lat,$current_lng,$withinkmradius,$year,$month){
		$this->load->model('location');
		//$ssam_data_results = $this->location->getSSAMdata_byYear();
		$ssam_data_results = $this->location->getNearby100kmPointsWithYearAndMonth($current_lat,$current_lng,$withinkmradius,$year,$month);
		$geojson = array(
   			'type'      => 'FeatureCollection',
   			'features'  => array()
		);

		# Loop through rows to build feature arrays
		foreach ($ssam_data_results as $row) {
			$chlor_a = $sst = $sal = 0;
			if($row['chlor_a'] != NULL) {
				$chlor_a = floatval($row['chlor_a']);
			}
			if($row['sst'] != NULL) {
				$sst = floatval($row['sst']);
			}
			if($row['salinity'] != NULL) {
				$sal = floatval($row['salinity']);
			}

		    $properties = array(
		    	//"name"=> "Id".$row['id'].' Coords_1:'.$row['latlng1'],
		    	"name"=> $row['latlng1'],
        		"density"=>  $chlor_a,
        		"sst" => $sst,
        		"salinity" => $sal,
        	);

		    $lat1 = explode(',',$row['latlng1']);
		    $lat1_f = array(floatval($lat1[1]),floatval($lat1[0]));

		    $lat2 = explode(',',$row['latlng2']);
		    $lat2_f = array(floatval($lat2[1]),floatval($lat2[0]));

		    $lat3 = explode(',',$row['latlng3']);
		    $lat3_f = array(floatval($lat3[1]),floatval($lat3[0]));

		    $lat3 = explode(',',$row['latlng3']);
		    $lat3_f = array(floatval($lat3[1]),floatval($lat3[0]));

		    $lat4 = explode(',',$row['latlng4']);
		    $lat4_f = array(floatval($lat4[1]),floatval($lat4[0]));

        	$coords = array($lat1_f,$lat2_f,$lat4_f,$lat3_f,$lat1_f);

        	$main_coords = array($coords);
        	$geometry = array(
		    	"type"=> "Polygon",
        		"coordinates" => $main_coords
        	);

		    $feature = array(
		         'type' => 'Feature',
		         'id' => $row['id'],
		         'properties' => $properties,
		         'geometry' => $geometry
		    );

			//Latitude: 10.719984036345847
			//Longitude: 122.5250244140625
		    # Add feature arrays to feature collection array
		    array_push($geojson['features'], $feature);
		}

			//echo $this->distance(32.9697, -96.80322, 29.46786, -98.53506, "K"); echo '<br/>';
			//echo $this->distance(10.720658697749604, 122.94868469238281, 10.719984036345847, 122.5250244140625, "K");
		echo json_encode($geojson);
		//echo count($geojson['features']);

	}

	function get_sst_data_with_location($current_lat,$current_lng,$withinkmradius,$year,$month){
		$this->load->model('location');
		//$ssam_data_results = $this->location->getSSAMdata_byYear();
		$ssam_data_results = $this->location->getNearby100kmPointsWithYearAndMonth($current_lat,$current_lng,$withinkmradius,$year,$month);
		$geojson = array(
   			'type'      => 'FeatureCollection',
   			'features'  => array()
		);

		# Loop through rows to build feature arrays
		foreach ($ssam_data_results as $row) {
			$chlor_a = $sst = $sal = 0;
			if($row['chlor_a'] != NULL) {
				$chlor_a = floatval($row['chlor_a']);
			}
			if($row['sst'] != NULL) {
				$sst = floatval($row['sst']);
			}
			if($row['salinity'] != NULL) {
				$sal = floatval($row['salinity']);
			}

		    $properties = array(
		    	//"name"=> "Id".$row['id'].' Coords_1:'.$row['latlng1'],
		    	"name"=> $row['latlng1'],
        		"density"=>  $chlor_a,
        		"sst" => $sst,
        		"salinity" => $sal,
        	);

		    $lat1 = explode(',',$row['latlng1']);
		    $lat1_f = array(floatval($lat1[1]),floatval($lat1[0]));

		    $lat2 = explode(',',$row['latlng2']);
		    $lat2_f = array(floatval($lat2[1]),floatval($lat2[0]));

		    $lat3 = explode(',',$row['latlng3']);
		    $lat3_f = array(floatval($lat3[1]),floatval($lat3[0]));

		    $lat3 = explode(',',$row['latlng3']);
		    $lat3_f = array(floatval($lat3[1]),floatval($lat3[0]));

		    $lat4 = explode(',',$row['latlng4']);
		    $lat4_f = array(floatval($lat4[1]),floatval($lat4[0]));

        	$coords = array($lat1_f,$lat2_f,$lat4_f,$lat3_f,$lat1_f);

        	$main_coords = array($coords);
        	$geometry = array(
		    	"type"=> "Polygon",
        		"coordinates" => $main_coords
        	);

		    $feature = array(
		         'type' => 'Feature',
		         'id' => $row['id'],
		         'properties' => $properties,
		         'geometry' => $geometry
		    );

		    # Add feature arrays to feature collection array
		    array_push($geojson['features'], $feature);
		}

		//echo $this->distance(32.9697, -96.80322, 29.46786, -98.53506, "K");
		echo json_encode($geojson);

	}

	function get_sal_data_with_location($current_lat,$current_lng,$withinkmradius,$year,$month){
		$this->load->model('location');
		//$ssam_data_results = $this->location->getSSAMdata_byYear();
		$ssam_data_results = $this->location->getNearby100kmPointsWithYearAndMonth($current_lat,$current_lng,$withinkmradius,$year,$month);
		$geojson = array(
   			'type'      => 'FeatureCollection',
   			'features'  => array()
		);

		# Loop through rows to build feature arrays
		foreach ($ssam_data_results as $row) {
			$chlor_a = $sst = $sal = 0;
			if($row['chlor_a'] != NULL) {
				$chlor_a = floatval($row['chlor_a']);
			}
			if($row['sst'] != NULL) {
				$sst = floatval($row['sst']);
			}
			if($row['salinity'] != NULL) {
				$sal = floatval($row['salinity']);
			}

		    $properties = array(
		    	//"name"=> "Id".$row['id'].' Coords_1:'.$row['latlng1'],
		    	"name"=> $row['latlng1'],
        		"density"=>  $chlor_a,
        		"sst" => $sst,
        		"salinity" => $sal,
        	);

		    $lat1 = explode(',',$row['latlng1']);
		    $lat1_f = array(floatval($lat1[1]),floatval($lat1[0]));

		    $lat2 = explode(',',$row['latlng2']);
		    $lat2_f = array(floatval($lat2[1]),floatval($lat2[0]));

		    $lat3 = explode(',',$row['latlng3']);
		    $lat3_f = array(floatval($lat3[1]),floatval($lat3[0]));

		    $lat3 = explode(',',$row['latlng3']);
		    $lat3_f = array(floatval($lat3[1]),floatval($lat3[0]));

		    $lat4 = explode(',',$row['latlng4']);
		    $lat4_f = array(floatval($lat4[1]),floatval($lat4[0]));

        	$coords = array($lat1_f,$lat2_f,$lat4_f,$lat3_f,$lat1_f);

        	$main_coords = array($coords);
        	$geometry = array(
		    	"type"=> "Polygon",
        		"coordinates" => $main_coords
        	);

		    $feature = array(
		         'type' => 'Feature',
		         'id' => $row['id'],
		         'properties' => $properties,
		         'geometry' => $geometry
		    );

		    # Add feature arrays to feature collection array
		    array_push($geojson['features'], $feature);
		}

		//echo $this->distance(32.9697, -96.80322, 29.46786, -98.53506, "K");
		echo json_encode($geojson);

	}

	function distance($lat1, $lon1, $lat2, $lon2, $unit) {

	  $theta = $lon1 - $lon2;
	  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
	  $dist = acos($dist);
	  $dist = rad2deg($dist);
	  $miles = $dist * 60 * 1.1515;
	  $unit = strtoupper($unit);

	  if ($unit == "K") {
	    return ($miles * 1.609344);
	  } else if ($unit == "N") {
	      return ($miles * 0.8684);
	    } else {
	        return $miles;
	      }
	}

	function db_distance_test(){
		$this->load->model('location');
		echo $this->location->getDistanceBetweenPointsNew(21.750033334,112.000166666,10.71998, 122.52502,'Km');
	}

	function db_100_test(){

		$this->load->model('location');
		$result = $this->location->getNearby100kmPointsWithYearAndMonth(10.709189249411077, 122.5579833984375,100,2015,12);

		echo '<pre>';
		print_r($result);
	}
	//now i can get nearby 100km points
	//NEXT CREATE Function that is flexible


	function import_dump(){
		$data['content'] = 'bigdump';
		$this->load->view('includes/template',$data);
	}

}