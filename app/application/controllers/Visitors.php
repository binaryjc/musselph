<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Visitors extends CI_Controller {

	function __construct(){
		parent::__construct();
		//Load Needed Models and Libraries

	}

	public function index(){
		$data['content'] = 'welcome_message';
		$data['page_index'] = 'home';
		$this->load->view('includes/template',$data);
	}
	public function sites(){
		$data['content'] = 'welcome_message';
		$data['page_index'] = 'home';
		$this->load->view('includes/template',$data);
	}
	public function about(){
		$data['content'] = 'about';
		$data['page_index'] = 'about';
		$this->load->view('includes/template',$data);
	}
	public function heatmap(){

		$this->load->model('location');
		$data['heatmap_areas'] = $this->location->getAllHeatLocations();
		$data['page_index'] = 'heatmap';
		$data['content'] = 'heatmap_leaflet';
		$this->load->view('includes/template',$data);
	}
	public function heatmap_test(){

		$this->load->model('location');
		$data['heatmap_areas'] = $this->location->getAllHeatLocations();
		$data['page_index'] = 'heatmap';
		$data['content'] = 'heatmap_leaflet2';
		$this->load->view('includes/template',$data);
	}
	public function heatmap_download(){
		$this->load->model('location');
		$data['heatmap_areas'] = $this->location->getAllHeatLocations();
		$data['page_index'] = 'heatmap';
		$data['content'] = 'heatmap_download';
		$this->load->view('includes/print_template',$data);
	}
	public function water_parameters(){
		$this->load->model('location');
		$data['page_index'] = 'water_parameters';
		$data['content'] = 'water_parameters';
		$this->load->view('includes/template',$data);
	}

	public function suitability_calculator(){
		$this->load->model('location');
		$data['page_index'] = 'calculator';
		$data['modal'] = 'calculator_modal';
		$data['content'] = 'calculator';
		$this->load->view('includes/template',$data);
	}

	function search_osm_locations(){
		//if($_POST){
			//$json = file_get_contents('http://nominatim.openstreetmap.org/search?format=json&limit=3&q=iloilo');
			$json = file_get_contents('http://nominatim.openstreetmap.org/search?email=myemail@myserver.com&format=json&limit=5&q='.$_POST['keyword']);
    		//$arrayt = json_decode($json,TRUE);
    		//echo '<pre>';
			//print_r($json);

			//$results = json_encode($arrayt);
			echo $json;
    		//echo '<pre>';print_r($arrayt);
		//}
		/* Create a stream
		$opts = array('http'=>array('header'=>"Content-Type: application/x-www-form-urlencoded\r\n"));
		$context = stream_context_create($opts);

		// Open the file using the HTTP headers set above
		$file = file_get_contents('http://nominatim.openstreetmap.org/search?format=json&limit=5&q='.$_POST['keyword'], false, $context);
		echo $file;
		*/
	}


	//downloading excel consultants
	function download_map($sitename,$parameter){
		$this->load->helper('download');
		$data = file_get_contents(base_url() . 'resources/images/sites/'.$sitename.'/'.$parameter.".jpg"); // Read the file's contents
		$name =	$sitename.'_'.$parameter.".jpg";
		force_download($name,$data);
	}

	function map_gallery(){
		$this->load->model('location');
		$data['sites'] = $this->location->getAllLocations();
		$data['page_index'] = 'gallery';
		$data['content'] = 'gallery';
		$data['gallery_css'] = 'gallery_css_on';
		$data['script'] = 'gallery_script';
		$this->load->view('includes/template',$data);
	}

}