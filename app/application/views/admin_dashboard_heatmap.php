
    <script src='<?=base_url()?>resources/js/mapbox.js'></script>
    <!--<script src='<?=base_url()?>resources/js/zoom.js'></script>-->
    <link href='<?=base_url()?>resources/css/mapbox.css' rel='stylesheet' />

    <script src='<?=base_url()?>resources/js/mapbox-locate.min.js'></script>
    <link href='<?=base_url()?>resources/css/mapbox-locate.css' rel='stylesheet' /> 
    <!--<link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-locatecontrol/v0.43.0/L.Control.Locate.mapbox.css' rel='stylesheet' />-->
    <!--[if lt IE 9]>
    <link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-locatecontrol/v0.43.0/L.Control.Locate.ie.css' rel='stylesheet' />
    <![endif]-->
    <link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-locatecontrol/v0.43.0/css/font-awesome.min.css' rel='stylesheet' />

    <!--admin only-->
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap4.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />

<div class="fullwidth full-page-header bg_mussel_2 coverbg bg-position-center">
    <div class="container">
        <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <h1 class="light-text maxwell">
                            Admin Dashboard <small class="light-text">Area Heat Maps</small>
                        </h1>
                    </div>
                </div>
        </div>
    </div>
</div>
	<div class="container pb60">
		<div class="row">
			<div class="col-md-12">

				<!--ERROR PANEL -->
					<?php
					$success = $this->session->flashdata('success');
					$errors = $this->session->flashdata('errors');
					if ($success || $errors):
					?>
					    <div class="general-notification trans-dark">
					        <div class="flexhorizontal">
					            <div class="col-xs-12">
					                <div class="row">
					                    <?php 
					                        if ($success):
					                      ?>
					                                  <div class="pale-bg roundborder p15 mb15">
					                                    <h4 class="text-success"><span class="pr5"><i class="fa fa-smile-o"></i></span> 
					                                      <?= $success; ?></h4>
					                                  </div>
					                      <?php     
					                        endif;
					                        if ($errors):
					                            foreach($errors as $error):
					                                if ($error != ""):
					                        ?>
					                                            <div class="pale-bg roundborder p15 mb15">
					                                                <h4 class="text-danger"><span class="pr5"><i class="fa fa-frown-o"></i></span> 
					                                              <?= $error; ?></h4>
					                                            </div>
					                        <?php
					                                endif;
					                            endforeach;
					                        endif;
					                    ?>
					                </div>
					            </div>
					        </div>
					    </div>
					<?php
					endif;
					?>
				<!--END ERROR PANEL -->

				<div class="row">
					<div class="col-md-12">
						<table class="table table-hover table-bordered" id="area_datatable">
							<thead>
								<tr>
									<th>#</th>
									<th>Name</th>
									<th>Content</th>
									<th>Latitude-Longitude Arrays</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody id="locations_table">
							</tbody>
						</table>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
						<h3>
							
						</h3>
						<div>
							<a href="#panel-element-14258" class="btn btn-default btn-sm btn-left" data-toggle="collapse"><span class="pr5"><i class="fa fa-plus-circle text-primary"></i></span> Add New Heatmap Location</a>	
						</div>
						<br/>
						<div class="panel-group" id="panel-121069">
							<div class="panel panel-default">
								<div id="panel-element-14258" class="panel-collapse collapse in">
									<div class="panel-body">
										
			    					<?php
									$attributes = array('role' => 'form');
									echo form_open_multipart('admins/add_heat_location');
									?>
											<div class="form-group">
												<label for="heat_loc_name">
													Area Name
												</label>
												<input class="form-control" id="heat_loc_name" name="heat_loc_name" required type="text" />
											</div>
											<div class="form-group">
												<label for="loc_content">
													Area Description
												</label>
												<input type="hidden" name="heat_loc_type" value="normal"/>
												<textarea class="form-control mce" id="heat_loc_content" name="heat_loc_content"  required></textarea>
											</div>

											<div class="form-group">
												<label for="heat_loc_latlng_arrays">
													Latitude-Longitude-Intensity Point Group
												</label>
												<textarea class="form-control mce" id="heat_loc_latlng_arrays" name="heat_loc_latlng_arrays" placeholder="Use map to encode data here" required></textarea>
												
												<div class="clearfix ptb15">
													<div class="col-xs-12 col-sm-6">
														<label for="heat_loc_radius">
															Intensity Radius
														</label>
														<input type="range" min="0" max="100" step="5" value="15" id="heat_loc_radius" name="heat_loc_radius" onchange="updateTextInput(this.value);">
													</div>
													<div class="col-xs-12 col-sm-6">
														<p></p>
														<input class=" form-control col-xs-12 col-sm-6 col-md-3" type="text" id="textInput" value="15">
													</div>
												</div>

												<div id='map' style="height:70vh;width:100%;"></div>
												<br/>
												<div>
												    <div class="panel panel-info">
												      <div class="panel-heading">How to add a point area:</div>
												      <div class="panel-body">
												      	<ol>
												      		<li>Set point intensity</li>
												      		<li>Use blue marker to navigate</li>
												      		<li>Click the blue marker to add current location and intensity to group</li>
												      	</ol>
												      </div>
												    </div>
													<small></small>
												</div>
											</div>

											<button type="submit" class="btn btn-default">
												Submit
											</button>
									<?= form_close(); ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<br>

			    <!-Admin Edit AREA-->
			    <div class="modal custom-modal fade" id="modal-container-edit-location" role="dialog">
			      	<div class="modal-dialog modal-lg">
			      		<div class="modal-content">
			      			<div class="modal-header bg-primary roundtop relative">
			      		      <div class="clearfix">
			      		        <h4 class="modal-title" id="consultancy-application-modal-label"><span class="pr10"></span>Edit Heat Area</h4>
			      		      </div>
			      		    </div>
			      			<div class="modal-body white-bg roundbottom">
			      				
								<div class="clearfix">
									<div class="error-message"></div>
									<?php
									$attributes = array('role' => 'form');
									echo form_open('admins/edit_heat_location');
									?>
										<div class="form-group">
											<label for="heat_edit_name">
												Area Name
											</label>
											<input class="form-control" id="heat_edit_name" name="heat_edit_name" required type="text" />
										</div>
										<div class="form-group">
											<label for="heat_edit_name">
												Area Description
											</label>
											<textarea class="form-control mce" id="heat_edit_content" name="heat_edit_content"  required></textarea>
										</div>
										<div class="form-group">
											<label for="heat_edit_latlng">
												Latitude-Longitude-Intensity Point Group
											</label>
											<textarea class="form-control mce" id="heat_edit_latlng" name="heat_edit_latlng" required> </textarea>

											<div class="clearfix ptb15">
												<div class="col-xs-12 col-sm-6">
													<label for="heat_edit_radius">
														Intensity Radius
													</label>
													<input type="range" min="0" max="100" step="5" value="0" id="heat_edit_radius" name="heat_edit_radius" onchange="updateTextInput_modal(this.value);">
												</div>
												<div class="col-xs-12 col-sm-6">
													<p></p>
													<input class=" form-control col-xs-12 col-sm-6 col-md-3" type="text" id="textInput2" value="0">
												</div>
											</div>

										<div class="text-center">
											<div class="center-block">
												<div class="btn-group">
													<input type="hidden" name="heat_selected_id" id="heat_selected_id">
													<button type="submit" class="btn btn-default btn-sm register-admin-account" name="btn-update_admin_account"><span class="pr5"><i class="fa fa-floppy-o text-success"></i></span>Update</button>
													<!-- <a href="#" class="btn btn-default btn-sm save-edit-faq-entry"><span class="pr5"><i class="fa fa-floppy-o text-success"></i></span>Register</a> -->
													<a href="#" class="btn btn-default btn-sm cancel-edit-faq-entry" data-dismiss="modal"><span class="pr5"><i class="fa fa-ban text-danger"></i></span>Cancel</a>
												</div>	
											</div>
										</div>
									<?php
									echo form_close();
									?>
								</div>
			      			</div>
			      			</div>
			      		</div>
			      	</div>
				</div>

		</div>
	</div>





<script>
	function updateTextInput(val) {
          document.getElementById('textInput').value=val; 
    }
	function updateTextInput_modal(val) {
          document.getElementById('textInput2').value=val; 
    }

    var admin_getAllLocations = function(){
        <?php
        $getAllLocations = base_url()."mapbox/getAllHeatLocations/";
        ?>

		$.getJSON("<?= $getAllLocations; ?>", function (data){
		    var total_queries = data.length;
		    $('#count_total_queries').text(total_queries); 
		        if(total_queries <= 0){
		          $("#locations_table").append('<tr>'+
		              '<td class="text-center" colspan="8">'+
		                'No Locations found.'+
		              '</td>'+
		            '</tr>');
		        }else{
		          var num_text = 0;
		          $.each(data, function(id,entry_data) { 
					num_text = num_text+1;

		            $("#locations_table").append('<tr>'+
		               '<td>'+num_text+'</td>'+
		                '<td class="text-center">'+entry_data['name']+'</td>'+
		                '<td class="text-center">'+entry_data['desc']+
		                	'<br><a href="#modal-container-view-location-content" onclick="getSpecificLocContent('+entry_data['id']+');" role="button" class="" data-toggle="modal">View Full Content</a>'+
		                '</td>'+
		                '<td class="text-center">'+entry_data['latlng_array']+'</td>'+
		                '<td class="text-center">'+
		                	'<a href="#modal-container-edit-location" onclick="getSpecificLocation('+entry_data['id']+');" role="button" class="" data-toggle="modal">Edit</a><br/>'+
		                	'<a href="" onclick="deleteSpecificLocation('+entry_data['id']+');" role="button" class="">Delete</a>'+
		                '</td>'+
		            '</tr>');   
		          });
		        }
			$('#area_datatable').DataTable({"scrollX": true,"oLanguage": {"sSearch": "Result Filter: "},"lengthMenu": [ [10, 25, 50,100,500,1000, -1], [10, 25, 50,100,500,1000, "All"] ]});
		});


    }

	var getSpecificLocation = function(admin_selected_id){
	  <?php
	  	$get_administrators_link = base_url()."mapbox/get_specific_heat_location/";
	  ?>
	 
	  console.log("<?= $get_administrators_link; ?>"+admin_selected_id);
	  $.getJSON("<?= $get_administrators_link; ?>"+admin_selected_id, function (data){
	      $.each(data, function(id,entry_data) {
	        $("#heat_selected_id").val(entry_data["id"]);
	        $("#heat_edit_name").val(entry_data["name"]);
	        $("#heat_edit_latlng").html(entry_data['latlng_array']);
	        $("#heat_edit_content").val(entry_data['desc']);


	        //set heatmap area

	  	});
	  });


	}

	
	var deleteSpecificLocation = function(id){
	<?php
	 $deleteIndividualUser=  base_url()."admins/delete_heat_location/";
	?>	
	    var delete_individual_link='<?=$deleteIndividualUser;?>'+id;
		console.log(delete_individual_link);
	    var r = confirm("Are you Sure?");
	    if (r == true) {
	      window.location.assign(delete_individual_link);
	  } else {
	      txt = "You pressed Cancel!";
	  }
	}
	
    $(document).ready(function(){
        admin_getAllLocations();
    });


	var map = L.map('map').setView([10.722345344678637,122.5605583190918], 6);
      //L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
      L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
          attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
          maxZoom: 20,
          id: 'mapbox.emerald',
          accessToken: 'pk.eyJ1IjoiYmluYXJ5amMiLCJhIjoiY2lrc3p1Nm5nMDAxeHVnbTM4NmZtYjJkZCJ9.BSH4W1Y1GssKFpB10hjQWw'
      }).addTo(map);

    //coordinate setter
    var marker2 = L.marker([10.722345344678637, 122.5605583190918], {
        draggable: true
    }).addTo(map);
    //marker2.on('dragend', ondragend);
    marker2.on('click', addtotextarea);
    //ondragend();
    /*
    function ondragend() {
        var m = marker2.getLatLng();
        $('#heat_loc_latlng_arrays').append('['+m.lat+","+m.lng+'],');
    }
     */
    function addtotextarea() {
        var m = marker2.getLatLng();
        var loc_radius = $("#heat_loc_radius").val();
        $('#heat_loc_latlng_arrays').append('{lat:'+m.lat+",lng:"+m.lng+',radius:'+loc_radius+'},');
    }


</script>
