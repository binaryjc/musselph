        <!-- Modal -->
        <div id="instructions" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">How to use the calculator</h4>
              </div>
              <div class="modal-body">
                 <ul>
                        <li>Toggle the switch <strong>Yes</strong> or <strong>No</strong> depending on your location/site</li>
                        <li>Your answers are automaticaly evaluated</li>
                        <li>Check your results at the bottom of the page</li>
                        <li>Click the 'Download' button to save a copy your results</li>
                    </ul>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>

          </div>
        </div>
        <div id="optimum" class="modal fade mt20" role="dialog">
          <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Optimum parameters for green mussel</h4>
              </div>
              <div class="modal-body">
                 <ul>
                        <li><strong>Salinity :</strong> 25 - 33 ppt</li>
                        <li><strong>Temperature :</strong> 25 -33°C</li>
                        <li><strong>Salinity :</strong> 25 - 33 ppt</li>
                        <li><strong>pH :</strong> 7 -8</li>
                        <li><strong>Dissolved Oxygen :</strong> 5-7 mgl-1</li>
                    </ul>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>

          </div>
        </div>