
    <script src='<?=base_url()?>resources/js/mapbox.js'></script>
    <!--<script src='<?=base_url()?>resources/js/zoom.js'></script>-->
    <link href='<?=base_url()?>resources/css/mapbox.css' rel='stylesheet' />

    <script src='<?=base_url()?>resources/js/mapbox-locate.min.js'></script>
    <link href='<?=base_url()?>resources/css/mapbox-locate.css' rel='stylesheet' /> 
    <!--<link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-locatecontrol/v0.43.0/L.Control.Locate.mapbox.css' rel='stylesheet' />-->
    <!--[if lt IE 9]>
    <link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-locatecontrol/v0.43.0/L.Control.Locate.ie.css' rel='stylesheet' />
    <![endif]-->
    <link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-locatecontrol/v0.43.0/css/font-awesome.min.css' rel='stylesheet' />

    <!--admin only-->
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap4.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />

<div class="fullwidth full-page-header bg_mussel_2 coverbg bg-position-center">
    <div class="container">
        <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <h1 class="light-text maxwell">
                            Admin Dashboard <small class="light-text">Area Parameters</small>
                        </h1>
                    </div>
                </div>
        </div>
    </div>
</div>
	<div class="container pb60">
		<div class="row">
			<div class="col-md-12">

				<!--ERROR PANEL -->
					<?php
					$success = $this->session->flashdata('success');
					$errors = $this->session->flashdata('errors');
					if ($success || $errors):
					?>
					    <div class="general-notification trans-dark">
					        <div class="flexhorizontal">
					            <div class="col-xs-12">
					                <div class="row">
					                    <?php 
					                        if ($success):
					                      ?>
					                                  <div class="pale-bg roundborder p15 mb15">
					                                    <h4 class="text-success"><span class="pr5"><i class="fa fa-smile-o"></i></span> 
					                                      <?= $success; ?></h4>
					                                  </div>
					                      <?php     
					                        endif;
					                        if ($errors):
					                            foreach($errors as $error):
					                                if ($error != ""):
					                        ?>
					                                            <div class="pale-bg roundborder p15 mb15">
					                                                <h4 class="text-danger"><span class="pr5"><i class="fa fa-frown-o"></i></span> 
					                                              <?= $error; ?></h4>
					                                            </div>
					                        <?php
					                                endif;
					                            endforeach;
					                        endif;
					                    ?>
					                </div>
					            </div>
					        </div>
					    </div>
					<?php
					endif;
					?>
				<!--END ERROR PANEL -->

				<div class="row">
					<div class="col-md-12">
						<table class="table table-hover table-bordered" id="area_datatable">
							<thead>
								<tr>
									<th>#</th>
									<th>Name</th>
									<th>Content</th>
									<th>Latitude-Longitude</th>
									<th>Dissolved Oxygen (PPM)</th>
									<th>Salinity (PPT)</th>
									<th>Temperature (ᵒC)</th>
									<th>Chlorophyll A (µg/L)</th>
									<th>Reactive Phosphorous (ppm)</th>
									<th>Reactive Nitrate (ppm)</th>
									<th>Particulate Organic Matter (POM)</th>
									<th>Turbidity</th>
									<th>pH</th>
									<th>Fecal Coliform</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody id="locations_table">
							</tbody>
						</table>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
						<h3>
							
						</h3>
						<div>
							<a href="#panel-element-14258" class="btn btn-default btn-sm btn-left" data-toggle="collapse"><span class="pr5"><i class="fa fa-plus-circle text-primary"></i></span> Add New Location</a>	
						</div>
						<br/>
						<div class="panel-group" id="panel-121069">
							<div class="panel panel-default">
								<div id="panel-element-14258" class="panel-collapse collapse in">
									<div class="panel-body">
										
			    					<?php
									$attributes = array('role' => 'form');
									echo form_open_multipart('admins/add_location');
									?>
											<div class="form-group">
												<label for="loc_name">
													Area Name
												</label>
												<input class="form-control" id="loc_name" name="loc_name" required type="text" />
											</div>
											<div class="form-group">
												<label for="loc_content">
													Area Description
												</label>
												<input type="hidden" name="loc_type" value="normal"/>
												<textarea class="form-control mce" id="loc_content" name="loc_content"  required></textarea>
											</div>
											<label for="loc_oxygen">
												Water Paramerters:
											</label>
											<div class="col-xs-12">
												<div class="form-group col-xs-12 col-md-6">
													<label for="loc_oxygen">
														Dissolved Oxygen (PPM) (DO%)
													</label>
													<input class="form-control" id="loc_oxygen" name="loc_oxygen" required type="text" />
													<label for="loc_oxygen_2">
														Dissolved Oxygen (DO) mg/L
													</label>
													<input class="form-control" id="loc_oxygen_2" name="loc_oxygen_2" required type="text" />
													<label for="loc_salinity">
														Salinity (PPT)
													</label>
													<input class="form-control" id="loc_salinity" name="loc_salinity" required type="text" />
													<label for="loc_temperature">
														Temperature (ᵒC)
													</label>
													<input class="form-control" id="loc_temperature" name="loc_temperature" required type="text" />
													<label for="loc_chlorophyll_a">
														Chlorophyll A (µg/L)
													</label>
													<input class="form-control" id="loc_chlorophyll_a" name="loc_chlorophyll_a" required type="text" />
													<label for="loc_reactive_phosphorous">
														Reactive Phosphorous (ppm)
													</label>
													<input class="form-control" id="loc_reactive_phosphorous" name="loc_reactive_phosphorous" required type="text" />
												</div>
												<div class="form-group col-xs-12 col-md-6">
													<label for="loc_reactive_nitrate">
														Reactive Nitrate (ppm)
													</label>
													<input class="form-control" id="loc_reactive_nitrate" name="loc_reactive_nitrate" required type="text" />
													<label for="loc_pom">
														Particulate Organic Matter (POM)
													</label>
													<input class="form-control" id="loc_pom" name="loc_pom" required type="text" />
													<label for="loc_turbidity">
														Turbidity
													</label>
													<input class="form-control" id="loc_turbidity" name="loc_turbidity" required type="text" />
													<label for="loc_ph">
														pH
													</label>
													<input class="form-control" id="loc_ph" name="loc_ph" required type="text" />
													<label for="loc_fecal_coliform">
														Fecal Coliform
													</label>
													<input class="form-control" id="loc_fecal_coliform" name="loc_fecal_coliform" required type="text" />
													<label for="loc_spc">
														Specific Conductance (SPC (-u)) 
													</label>
													<input class="form-control" id="loc_spc" name="loc_spc" required type="text" />
													<label for="loc_tds">
														Total Dissolved Solids (TDS) mg/L
													</label>
													<input class="form-control" id="loc_tds" name="loc_tds" required type="text" />
												</div>
											</div>

											<div class="form-group">
												<label for="loc_latlng">
													Latitude-Longitude
												</label>
												<input class="form-control" id="loc_latlng" name="loc_latlng" required type="text" />
												<p class="help-block">
													example: 10.723457472354175,122.55618631839751
												</p>
												<div id='map' style="height:50vh;width:100%;"></div> 
											</div>

											<button type="submit" class="btn btn-default">
												Submit
											</button>
									<?= form_close(); ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<br>

			    <!-Admin Edit AREA-->
			    <div class="modal custom-modal fade" id="modal-container-edit-location" role="dialog">
			      	<div class="modal-dialog modal-lg">
			      		<div class="modal-content">
			      			<div class="modal-header bg-primary roundtop relative">
			      		      <div class="clearfix">
			      		        <h4 class="modal-title" id="consultancy-application-modal-label"><span class="pr10"></span>Edit Water Area</h4>
			      		      </div>
			      		    </div>
			      			<div class="modal-body white-bg roundbottom">
			      				
								<div class="clearfix">
									<div class="error-message"></div>
									<?php
									$attributes = array('role' => 'form');
									echo form_open('admins/edit_location');
									?>
										<div class="form-group">
											<label for="loc_edit_name">
												Area Name
											</label>
											<input class="form-control" id="loc_edit_name" name="loc_edit_name" required type="text" />
										</div>
										<div class="form-group">
											<label for="loc_edit_content">
												Area Description
											</label>
											<input type="hidden" name="loc_type" value="normal"/>
											<textarea class="form-control mce" id="loc_edit_content" name="loc_edit_content"  required></textarea>
										</div>
										<label for="loc_oxygen">
											Water Paramerters:
										</label>
										<div class="col-xs-12">
											<div class="form-group col-xs-12 col-md-6">
												<label for="loc_edit_oxygen">
													Dissolved Oxygen (PPM)
												</label>
												<input class="form-control" id="loc_edit_oxygen" name="loc_edit_oxygen" required type="text" />
												<label for="loc_edit_oxygen_2">
													Dissolved Oxygen (DO) mg/L
												</label>
												<input class="form-control" id="loc_edit_oxygen_2" name="loc_edit_oxygen_2" required type="text" />
												<label for="loc_edit_salinity">
													Salinity (PPT)
												</label>
												<input class="form-control" id="loc_edit_salinity" name="loc_edit_salinity" required type="text" />
												<label for="loc_edit_temperature">
													Temperature (ᵒC)
												</label>
												<input class="form-control" id="loc_edit_temperature" name="loc_edit_temperature" required type="text" />
												<label for="loc_edit_chlorophyll_a">
													Chlorophyll A (µg/L)
												</label>
												<input class="form-control" id="loc_edit_chlorophyll_a" name="loc_edit_chlorophyll_a" required type="text" />
												<label for="loc_edit_reactive_phosphorous">
													Reactive Phosphorous (ppm)
												</label>
												<input class="form-control" id="loc_edit_reactive_phosphorous" name="loc_edit_reactive_phosphorous" required type="text" />
											</div>
											<div class="form-group col-xs-12 col-md-6">
												<label for="loc_edit_reactive_nitrate">
													Reactive Nitrate (ppm)
												</label>
												<input class="form-control" id="loc_edit_reactive_nitrate" name="loc_edit_reactive_nitrate" required type="text" />
												<label for="loc_edit_pom">
													Particulate Organic Matter (POM)
												</label>
												<input class="form-control" id="loc_edit_pom" name="loc_edit_pom" required type="text" />
												<label for="loc_edit_turbidity">
													Turbidity
												</label>
												<input class="form-control" id="loc_edit_turbidity" name="loc_edit_turbidity" required type="text" />
												<label for="loc_edit_ph">
													pH
												</label>
												<input class="form-control" id="loc_edit_ph" name="loc_edit_ph" required type="text" />
												<label for="loc_edit_fecal_coliform">
													Fecal Coliform
												</label>
												<input class="form-control" id="loc_edit_fecal_coliform" name="loc_edit_fecal_coliform" required type="text" />
												<label for="loc_edit_spc">
													Specific Conductance (SPC (-u)) 
												</label>
												<input class="form-control" id="loc_edit_spc" name="loc_edit_spc" required type="text" />
												<label for="loc_edit_tds">
													Total Dissolved Solids (TDS) mg/L
												</label>
												<input class="form-control" id="loc_edit_tds" name="loc_edit_tds" required type="text" />
											</div>
										</div>

										<div class="form-group">
											<label for="loc_edit_latlng">
												Latitude-Longitude
											</label>
											<input class="form-control" id="loc_edit_latlng" name="loc_edit_latlng" required type="text" />
												<p class="help-block">
													Use marker on map to change location value
												</p>
												<div id="map_modal_wrapper">
													<div id='map_modal'></div> 
												</div>
										</div>
										<div class="text-center">
											<div class="center-block">
												<div class="btn-group">
													<input type="hidden" name="loc_selected_id" id="loc_selected_id">
													<button type="submit" class="btn btn-default btn-sm register-admin-account" name="btn-update_admin_account"><span class="pr5"><i class="fa fa-floppy-o text-success"></i></span>Update</button>
													<!-- <a href="#" class="btn btn-default btn-sm save-edit-faq-entry"><span class="pr5"><i class="fa fa-floppy-o text-success"></i></span>Register</a> -->
													<a href="#" class="btn btn-default btn-sm cancel-edit-faq-entry" data-dismiss="modal"><span class="pr5"><i class="fa fa-ban text-danger"></i></span>Cancel</a>
												</div>	
											</div>
										</div>
									<?php
									echo form_close();
									?>
								</div>
			      			</div>
			      			</div>
			      		</div>
			      	</div>
				</div>

		</div>
	</div>





<script>
	
    var admin_getAllLocations = function(){
        <?php
        $getAllLocations = base_url()."mapbox/getAllLocations/";
        ?>

		$.getJSON("<?= $getAllLocations; ?>", function (data){
		    var total_queries = data.length;
		    $('#count_total_queries').text(total_queries); 
		        if(total_queries <= 0){
		          $("#locations_table").append('<tr>'+
		              '<td class="text-center" colspan="8">'+
		                'No Locations found.'+
		              '</td>'+
		            '</tr>');
		        }else{
		          var num_text = 0;
		          $.each(data, function(id,entry_data) { 
					num_text = num_text+1;

		            $("#locations_table").append('<tr>'+
		               '<td>'+num_text+'</td>'+
		                '<td class="text-center">'+entry_data['name']+'</td>'+
		                '<td class="text-center">'+entry_data['content']+
		                '<td class="text-center">'+entry_data['latlng']+'</td>'+
		                '<td class="text-center">'+entry_data['dissolved_oxygen']+'</td>'+
		                '<td class="text-center">'+entry_data['salinity']+'</td>'+
		                '<td class="text-center">'+entry_data['temperature']+'</td>'+
		                '<td class="text-center">'+entry_data['chlorophyll_a']+'</td>'+
		                '<td class="text-center">'+entry_data['reactive_phosphorous']+'</td>'+
		                '<td class="text-center">'+entry_data['reactive_nitrate']+'</td>'+
		                '<td class="text-center">'+entry_data['pom']+'</td>'+
		                '<td class="text-center">'+entry_data['turbidity']+'</td>'+
		                '<td class="text-center">'+entry_data['ph']+'</td>'+
		                '<td class="text-center">'+entry_data['fecal_coliform']+'</td>'+
		                	'<br><a href="#modal-container-view-location-content" onclick="getSpecificLocContent('+entry_data['id']+');" role="button" class="" data-toggle="modal">View Full Content</a>'+
		                '</td>'+
		                '<td class="text-center">'+
		                	'<a href="#modal-container-edit-location" onclick="getSpecificLocation('+entry_data['id']+');" role="button" class="" data-toggle="modal">Edit</a><br/>'+
		                	'<a href="" onclick="deleteSpecificLocation('+entry_data['id']+');" role="button" class="">Delete</a>'+
		                '</td>'+
		            '</tr>');   
		          });
		        }
			$('#area_datatable').DataTable({"scrollX": true,"oLanguage": {"sSearch": "Result Filter: "},"lengthMenu": [ [10, 25, 50,100,500,1000, -1], [10, 25, 50,100,500,1000, "All"] ]});
		});


    }

	var getSpecificLocation = function(admin_selected_id){
	  <?php
	  	$get_administrators_link = base_url()."mapbox/get_specific_location/";
	  ?>
	 
	  console.log("<?= $get_administrators_link; ?>"+admin_selected_id);
	  $.getJSON("<?= $get_administrators_link; ?>"+admin_selected_id, function (data){
			$.each(data, function(id,entry_data) {
		        $("#loc_selected_id").val(entry_data["id"]);
		        $("#loc_edit_name").val(entry_data["name"]);
		        $("#loc_edit_latlng").val(entry_data['latlng']);
		        $("#loc_edit_content").val(entry_data['content']);
		        $("#loc_edit_oxygen").val(entry_data['dissolved_oxygen']);
		        $("#loc_edit_salinity").val(entry_data['salinity']);
		        $("#loc_edit_temperature").val(entry_data['temperature']);
		        $("#loc_edit_chlorophyll_a").val(entry_data['chlorophyll_a']);
		        $("#loc_edit_reactive_phosphorous").val(entry_data['reactive_phosphorous']);
		        $("#loc_edit_reactive_nitrate").val(entry_data['reactive_nitrate']);
		        $("#loc_edit_pom").val(entry_data['pom']);
		        $("#loc_edit_turbidity").val(entry_data['turbidity']);
		        $("#loc_edit_ph").val(entry_data['ph']);
		        $("#loc_edit_fecal_coliform").val(entry_data['fecal_coliform']);

			    //coordinate setter
			    var coordinates_input = document.getElementById('loc_edit_latlng');
			    //alert($("#loc_edit_latlng").val());//parseFloat
			    var b = $("#loc_edit_latlng").val().split(',');
			    console.log(b);

			    //map_modal.remove();
				var node  = document.getElementById("map_modal");
				node.parentNode.removeChild(node);

				var div = document.createElement("div");
				div.setAttribute("id", "map_modal");
				// as an example add it to the body
				document.getElementById("map_modal_wrapper").appendChild(div);

				var map_modal = L.map('map_modal').setView([parseFloat(b[0]),parseFloat(b[1])],6);
				

			      //L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
			      L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
			          attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
			          maxZoom: 20,
			          id: 'mapbox.emerald',
			          accessToken: 'pk.eyJ1IjoiYmluYXJ5amMiLCJhIjoiY2lrc3p1Nm5nMDAxeHVnbTM4NmZtYjJkZCJ9.BSH4W1Y1GssKFpB10hjQWw'
			      }).addTo(map_modal);

				  $('#modal-container-edit-location').on('shown.bs.modal', function(){
    					setTimeout(function() {
        				map_modal.invalidateSize();
   						}, 1);
					})

				//map_modal.setView([parseFloat(b[0]),parseFloat(b[1])],6);

			    var marker3 = L.marker([parseFloat(b[0]),parseFloat(b[1])], {
			        draggable: true
			    }).addTo(map_modal);
			    marker3.on('dragend', ondragend);
			    ondragend();
			    function ondragend() {
			        var m = marker3.getLatLng();
			        coordinates_input.value = m.lat+","+m.lng;
			    }

	      });
	  });


	}


	var deleteSpecificLocation = function(id){
	<?php
	 $deleteIndividualUser=  base_url()."admins/delete_location/";
	?>	
	    var delete_individual_link='<?=$deleteIndividualUser;?>'+id;
		console.log(delete_individual_link);
	    var r = confirm("Are you Sure?");
	    if (r == true) {
	      window.location.assign(delete_individual_link);
	  } else {
	      txt = "You pressed Cancel!";
	  }
	}

    $(document).ready(function(){
        admin_getAllLocations();
    });


	var map = L.map('map').setView([10.722345344678637,122.5605583190918], 6);
      //L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
      L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
          attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
          maxZoom: 20,
          id: 'mapbox.emerald',
          accessToken: 'pk.eyJ1IjoiYmluYXJ5amMiLCJhIjoiY2lrc3p1Nm5nMDAxeHVnbTM4NmZtYjJkZCJ9.BSH4W1Y1GssKFpB10hjQWw'
      }).addTo(map);

    //coordinate setter
    var coordinates_input = document.getElementById('loc_latlng');
    var marker2 = L.marker([10.722345344678637, 122.5605583190918], {
        draggable: true
    }).addTo(map);
    marker2.on('dragend', ondragend);
    ondragend();
    function ondragend() {
        var m = marker2.getLatLng();
        coordinates_input.value = m.lat+","+m.lng;
    }




</script>
