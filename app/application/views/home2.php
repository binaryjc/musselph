    <div class="homepage-hero-module">
    <div class="video-container">
        <div class="filter"></div>
        <video autoplay loop class="fillWidth">
            <source src="<?=base_url()?>resources/videos/pebbles.mp4" type="video/mp4" />Your browser does not support the video tag. I suggest you upgrade your browser.
            <source src="<?=base_url()?>resources/videos/All-set.webm" type="video/webm" />Your browser does not support the video tag. I suggest you upgrade your browser.
        </video>
       <!-- <div class="poster">
            <img src="<?=base_url()?>resources/images/All-set.jpg" alt="">
        </div>-->
        <div class="title-container">
            <div class="headline" id="headling_text_container">
                <!--Original logo height 80px-->
                <img class="affiliate_image" src="<?=base_url()?>resources/images/pcaarrd.png" alt="DOST-PCAARRD" title="DOST-PCAARRD">
                <img src="<?=base_url()?>resources/images/mussel_ph_logo_vintage.png">
                <img class="affiliate_image" src="<?=base_url()?>resources/images/upv.png" alt="UP Visayas" title="University of the Philippines Visayas">
                <div>
                    <h1 class="animate-intro">Bigger and Safer Mussels for Consumers</h1>
                    <h3 class="text-white text-thin">The DOST-PCAARRD Industry Strategic S&T Program for MusselInland Aquatic Resources Division</h3>
                    <h5 class="text-white text-thin"><a class="coverr-nav-item btn-track" href="<?=base_url()?>visitors/about" data-event="MondayScroll_Click" style="text-decoration: none; border-bottom: 1px dotted #fff; color: #fff;">Learn more about the project</a></h5>
                </div>                
                
            </div>

            <div class="text-center" id="loadBannerVideoSpinner" style="background-color: rgba(0, 0, 0, 0.5); padding: 10px; border-radius: 4px; display: none; margin-top: 20px;">
                <h5 class="text-thin text-primary">Loading Coverr... <i class="fa fa-circle-o-notch fa-spin"></i></h5>
            </div>
        </div>
    </div>
    </div>
        <!-- Rationale - pricing style design
    ================================================== -->
        <section id="pricing">

        <div class="row section-intro animate-this">
            <div class="col-twelve with-bottom-line">

                <h2>Rationale</h2>

                <p class="lead">
                    The mussel industry is an important component of the aquaculture sector in the Philippines. Mussels are a cheap source of protein and mussel farming provides additional income and livelihood to fisherfolk in many coastal areas. Mussel farming requires little capital investment.
                        <br/><br/>In spite of these advantages production of farmed mussels has not increased significantly in the past ten years (i.e from 13, 513 metric tons (MT) in 2001 to 22, 442 MT in 2011).  This can be traced to its low value and market demand due to poor sanitary quality of produce and the occurrence of red tides. Furthermore, the area of its culture is limited by to existing stake culture method, which is restricted to shallow muddy areas, the crudeness of the spat transport method available and the unpredictable supply of mussel seeds or spats. 
                        <br/><br/>The PCAARRD Industry Strategic S&T Program for mussel aims to help develop the mussel industry by enabling farmers to produce quality and safe cultured and processed mussel products. It will develop technologies to provide farmers with sustainable and reliable source of quality mussel seeds and enable expansion of production areas for mussel farming.
                    

                </p>

            </div>          
        </div>
       </section> <!-- Pricing -->
    <!-- Programs (main 3)
    ================================================== -->
    <section id="features">

            <div class="row section-intro group animate-this">  
            <div class="col-twelve with-bottom-line">

                <h2 class="">Our Programs</h2>

                <p class="lead">Lorem ipsum Tempor do Excepteur nulla aute deserunt reprehenderit aute commodo aliquip laboris velit eu esse exercitation ex fugiat pariatur aute consequat. Lorem ipsum Fugiat mollit quis qui. Lorem ipsum Duis Excepteur Duis magna fugiat incididunt qui id quis dolor nisi irure proident labore ut in incididunt eiusmod est anim.</p>

            </div>          
            </div>

            <div class="row features-wrap">
                                
                <div class="features-list block-1-3 block-s-1-2 block-tab-full">

                    <div class="bgrid feature animate-this">    

                        <span class="feature-count">01.</span>            

                       <div class="feature-content">

                    <h3>Program A</h3>

                            <p>Enhancement of Hatchery and Nursery Practices for a Reliable Supply of Quality Seeds for the Green Mussel (Perna viridis) Farming
                           </p>
                                        
                        </div>               

                    </div> <!-- /bgrid -->

                    <div class="bgrid feature animate-this">    

                        <span class="feature-count">02.</span>            

                       <div class="feature-content">

                    <h3>Program B</h3>

                            <p>Improved Grow-Out Technology for Sustainable Mussel Industry
                           </p>
                                        
                        </div>               

                    </div> <!-- /bgrid -->

                    <div class="bgrid feature animate-this">    

                        <span class="feature-count">03.</span>            

                       <div class="feature-content">

                    <h3>Program C</h3>

                            <p>Post Harvest Program for Sustainable, High Quality and Safe Mussel (Perna viridis) Products
                           </p>
                                        
                        </div>               

                    </div> <!-- /bgrid -->

                </div> <!-- /features-list -->              

            </div> <!-- /features-wrap -->
            
    </section> <!-- /features -->
    
    <!-- PROGRAM A PROJECTS
    ================================================== -->
    <section id="infos">
        <!--A1-->
        <div class="info-entry">

            <div class="half-grey"></div>

            <div class="row info-entry-content">

                <div class="media-wrap">
                    <div class="media animate-this"  data-animate="fadeInRight">
                        <img src="<?=base_url()?>resources/images/projects/A1/Mussel_hatchery_building.png" alt="">
                    </div>                                      
                </div>

                <div class="col-six text-part">                 
                    
                    <h5 class="animate-this" data-animate="fadeInLeft">Program A</h5>
                    <h2 class="animate-this" data-animate="fadeInLeft">Simple Hatchery for Mussel Production </h2>

                    <p class="animate-this" data-animate="fadeInLeft">The green mussel (Perna viridis) is a traditional aquaculture species in the Philippines.  Its contribution to the country’s aquaculture production from 2013 to present plays around 25,660 tons. However, the increase and sustainable production of this species is hindered by the availability of seed (spat) supply.</p>

                    <a href="#" class="button animate-this" data-animate="fadeInLeft" data-toggle="modal" data-target="#a1-modal">Learn More</a>
                    <!-- A1 Modal -->
                    <div id="a1-modal" class="modal fade" role="dialog">
                      <div class="modal-dialog modal-lg">

                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <h4 class="modal-title">Simple Hatchery for Mussel Production </h4>
                          </div>
                          <div class="modal-body">
                            <p>
                                Dr. Liberato Laureta and Dr. Jane Amar <br/>
                                College of Fisheries and Ocean Sciences, University of the Philippines Visayas (UPV) <br/>
                                jhunlaureta@yahoo.com and janeamar09@gmail.com <br/>
                            </p>
                            
                            <strong>Project Description:</strong>
                            <p>
                                The green mussel (Perna viridis) is a traditional aquaculture species in the Philippines.  Its contribution to the country’s aquaculture production from 2013 to present plays around 25,660 tons. However, the increase and sustainable production of this species is hindered by the availability of seed (spat) supply. 
                                Producing spats under controlled conditions in hatchery will give growers greater security by reducing risks inherent to sourcing mussel spats from the wild.  A hatchery provides better opportunities to increase production levels by having spats available for a longer period of time.
                                The Philippine green mussel hatchery developed by the UP Visayas researchers is patterned (with slight modification) to the shellfish hatchery in Australia, New Zealand, and Japan. 
                                The operations include: 1) algal culture; 2) broodstock conditioning and spawning; 3) D-hinge larvae rearing; and 4) pediveliger rearing. 
                                The simultaneous culture of natural food organisms in plastic bags employed for mussels in Australia and New Zealand is also adopted with ample supply of freshwater and sea water in the hatchery operation. Sea water is pumped into the 8-ton concrete tank passing first into a sand filter for storage. 
                                To obtain the required water quality, the water from the storage tank is allowed to pass through a series of filtering apparatus (10 m, 5 m, and 1m) and then stored into the three elevated 1-ton polyethylene tanks.  In these water tanks, temperature and salinity are checked and set to the requirement of the hatchery before release.  The two tanks supply water requirements for the algal culture and broodstock conditioning.  After passing through an ultraviolet filter, the water from third tank, will fill the need for the larval rearing. 
                            </p>
                            <strong>Benefits</strong>
                            <ul>
                                <li>Reliable and continuous source of spat supply</li>
                                <li>Simple and relatively cheap hatchery facility</li>
                            </ul>
                            
                            <strong>Target Beneficiaries</strong>
                            <ul>
                                <li>Mussel growers </li>
                                <li>Hatchery operators</li>
                            </ul>

                            <strong>Locations</strong>
                            <ul>
                                <li>Iloilo</li>
                            </ul>
                            
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                        </div>

                      </div>
                    </div>                                
                </div>                  
                
            </div> <!-- /info-entry-content -->

        </div> <!-- /info-entry -->
        <!--A2-->
        <div class="info-entry">

            <div class="half-grey"></div>

            <div class="row info-entry-content">
            
                <div class="media-wrap">
                    <div class="media animate-this"  data-animate="fadeInRight">
                        <img src="<?=base_url()?>resources/images/projects/A2/Broodstock_rematuration_in_wild.jpg" alt="">
                    </div>                  
                </div>

                <div class="col-six text-part">                 
                    
                    <h5 class="animate-this" data-animate="fadeInRight">Program A</h5>
                    <h2 class="animate-this" data-animate="fadeInRight">Land-based and Water-based Nursery: Innovative Approach to Philippine Mussel Aquaculture </h2>

                    <p class="animate-this" data-animate="fadeInRight">In the Philippines, mussel culture starts with the installation of several spat collectors such as bamboo stakes in the mussel farms. These mussel farm structures (with attached spats) remain in the farm until the spats reach marketable sizes suitable for harvesting. </p>

                    <a href="#" class="button animate-this" data-animate="fadeInLeft" data-toggle="modal" data-target="#a2-modal">Learn More</a>
                    <!-- A2 Modal -->
                    <div id="a2-modal" class="modal fade" role="dialog">
                      <div class="modal-dialog modal-lg">

                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <h4 class="modal-title">Land-based and Water-based Nursery: Innovative Approach to Philippine Mussel Aquaculture </h4>
                          </div>
                          <div class="modal-body">
                            <p>
                                Dr. Liberato Laureta and Dr. Fiona L. Pedroso<br/>
                                College of Fisheries and Ocean Sciences, University of the Philippines Visayas (UPV)<br/>
                                jhunlaureta@yahoo.com and fionap_06@yahoo.com.ph<br/>
                            </p>
                            
                            <strong>Project Description:</strong>
                            <p>
                                In the Philippines, mussel culture starts with the installation of several spat collectors such as bamboo stakes in the mussel farms. These mussel farm structures (with attached spats) remain in the farm until the spats reach marketable sizes suitable for harvesting. 
                                However, collectors are insufficient when there is overabundance of spats. Many unattached spats die because of predation from crabs, fish, starfishes, and other mollusk species. Collecting spats using black nets can minimize mortality, and the collected spats can be brought to nursery for rearing.
                                Hatchery-produced spats also require nursery rearing before they are brought to the grow-out farms. Nurseries serve as interface between the hatcheries and grow-out phase. They aid in growing smaller seeds at low cost to sizes suitable for transfer to grow-out trays, bags, and nets.
                                There are two methods of holding hatchery-produced and wild mussel spats: 1) land-based nursery; and 2) water-based nursery. 
                                The land-based nursery is applicable even for smaller spats. The nursery set-up comprises of concrete tanks for rearing mussel and growing food.   Earthen ponds are also used to grow “green water” to supplement the food requirement of stock aside from cultured algae, such as Chaetoceros and Isochrysis.
                                Spats are allowed to set in collectors such as oyster cultch or nylon rope and grow until they are ready for transfer to grow-out farms or be placed inside mussel sock (mussock).  For spats larger than 7 mm, a medical cotton plaster formed into a sock with nylon rope (14 mm) as core is used for rearing. 
                                On the other hand, for the water-based nursery, a raft method of culture is preferred than long line method because it is easier to manage. Spats in oyster cultches or ropes and mussel socks with density of 300–500 individuals per meter are tied into the bamboo at 50 cm interval. The length of the mussock depends on the depth of the water during the lowest low tide.
                                                        </p>
                            <strong>Benefits</strong>
                            <ul>
                                <li>Readily available seeds for grow-out culture and transplantation to expansion areas</li>
                                <li>Relatively cheap and easy to manage nursery facilities</li>
                            </ul>
                            
                            <strong>Target Beneficiaries</strong>
                            <ul>
                                <li>Mussel farmers</li>
                                <li>Hatchery and nursery operators </li>
                            </ul>

                            <strong>Locations</strong>
                            <ul>
                                <li>Iloilo</li>
                                <li>Aklan</li>
                            </ul>
                            
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                        </div>

                      </div>
                    </div>   
                                
                </div>

            </div> <!-- /info-entry-content -->

        </div> <!-- /info-entry -->
        <!--A3-->
        <div class="info-entry">

            <div class="half-grey"></div>

            <div class="row info-entry-content">

                <div class="col-six text-part">                 
                    
                    <h5 class="animate-this" data-animate="fadeInLeft">Program A</h5>
                    <h2 class="animate-this" data-animate="fadeInLeft">Genetic characterization and selective breeding of the Philippine green mussel, Perna viridis </h2>

                    <p class="animate-this" data-animate="fadeInLeft">The Philippines.  Its contribution to the country’s aquaculture production from 2013 to present plays around 25,660 tons. However, the increase and sustainable production of this species is hindered by the availability of seed (spat) supply.</p>

                    <a href="#" class="button animate-this" data-animate="fadeInLeft" data-toggle="modal" data-target="#a3-modal">Learn More</a>
                    <!-- A3 Modal -->
                    <div id="a3-modal" class="modal fade" role="dialog">
                      <div class="modal-dialog modal-lg">

                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <h4 class="modal-title">Genetic characterization and selective breeding of the Philippine green mussel, Perna viridis</h4>
                          </div>
                          <div class="modal-body">
                            <p>
                                Dr. Philip Ian Padilla<br/>
                                College of Arts and Sciences, University of the Philippines Visayas (UPV)<br/>
                                pppadilla@upv.edu.ph<br/>
                            </p>
                            
                            <strong>Project Description:</strong>
                            <p>Mussel farmers rely on natural collection of broodstock and seed available in the wild. However, the variable nature of spat settlement, unreliable supply of wild spat, and occurrence of diseases can lead to problems in production. A selective breeding program must therefore accompany any hatchery operation geared towards improvement of broodstock and improved culture performance of spats. 
                            The selective breeding program for the Philippine green mussel, which, is being developed, uses broodstock from Samar, Palawan, Bataan, and Capiz. These sites were identified to produce high quality mussels in terms of condition index, meat yield, growth rate, and production, among other characteristics. Samples from all visited sites are characterized using COI, ITS, and 18s genes to determine the genetic divergence of mussels in the Philippines. Microsatellite markers are used to track parentage among the spats produced from the selected broodstock. This will confirm inheritance of desired characteristics from the parent stock.
                                The hatchery program for mussel aims to offer year-round supply of quality seed, select for faster growing mussels through progressive grading, and facilitate selective breeding to optimize growth rates, yields, and improve quality of marketable mussels in the Philippines.
                            </p>
                            <strong>Benefits</strong>
                            <ul>
                                <li>Readily available seeds for grow-out</li>
                                <li>Improved quality and culture performance of mussel</li>
                            </ul>
                            
                            <strong>Target Beneficiaries</strong>
                            <ul>
                                <li>Mussel farmers, fisheries planners, and policy makers</li>
                            </ul>

                            <strong>Locations</strong>
                            <ul>
                                <li>Negros Occidental, Pangasinan, Metro Manila, Zambales, Bataan, Samar, Aklan, and Capiz.</li>
                                <li>National Institute of Molecular Biology and Biotechnology (NIMBB), University of the Philippines Visayas, Miagao, Iloilo.</li>
                            </ul>
                            
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                        </div>

                      </div>
                    </div>   
                                
                </div>  

                <div class="media-wrap">
                    <div class="media animate-this"  data-animate="fadeInRight">
                        <img src="<?=base_url()?>resources/images/projects/A2/Hatchery_produced_spats_attaching_to_oyster_cultch.jpg" alt="">
                    </div>                                      
                </div>                
                
            </div> <!-- /info-entry-content -->

        </div> <!-- /info-entry -->
    </section> <!-- infos --> 
    
    <!-- PROGRAM A PROJECTS
    ================================================== -->
    
    <!-- SSAM
    ================================================== -->
    <section id="intro">

           <!-- <div class="shadow-overlay"></div> -->
           
           <div class="row intro-content">
            <div class="col-twelve">

                <h1 class="animate-intro">
                    SSAM
                </h1>   
                <h3 class="animate-intro">Suitability Assessment and Database Development <br/> for enhanced mussel culture management using geospatial technologies</h3>

                    <div class="buttons">
                        <a class="button stroke smoothscroll animate-intro" href="#features" title="">Open Application</a>
                    </div>                  

                    <img src="<?=base_url()?>resources/images/app-screenshot-big.jpg" alt="" class="animate-intro">              

                </div> <!-- /twelve -->                     
           </div> <!-- /row -->   

        </section> <!-- /intro -->

    <!-- MASTERS
    ================================================== -->
       <section id="testimonials">
        <div class="row testimonial-content">
            <div class="col-twelve">

                <h2 class="h01 animate-this">
                  Key Officials
               </h2>

                    <div id="testimonial-slider" class="flexslider animate-this">
                        <ul class="slides"> 
                           <li>
                                <p>&ldquo;Nulla vestibulum nisi et tortor sodales tempor. Maecenas ac quam non metus ullamcorper elementum vel tempus mauris. Integer volutpat est a scelerisque hendrerit.&rdquo;</p>
                                <p class="author-info">&mdash; Dr. Reynaldo V. Ebora</p>
                                <p class="author-info">Acting Executive Director <br/> r.ebora@pcaarrd.dost.gov.ph</p>
                            </li>                       
                            <li>
                                <p>&ldquo;Donec quis nunc ultricies, porta elit eget, elementum est. Maecenas elementum purus vitae mauris pellentesque scelerisque. Phasellus faucibus euismod volutpat&rdquo;</p>
                                <p class="author-info">&mdash; Dr. Dalisay DG. Fernandez</p>
                                <p class="author-info">IARD-Director<br/>d.fernandez@pcaarrd.dost.gov.ph</p>
                            </li>                       
                            <li>
                                <p>&ldquo;Cras nisi felis, semper pharetra turpis vitae, eleifend auctor metus. Proin ornare libero a turpis rutrum blandit. &rdquo;</p>
                                <p class="author-info">&mdash; Kitchie L. Tandang</p>
                                <p class="author-info">Mussel S&amp;T Manager <br/> kjltandang@gmail.com</p>

                            </li>
                        </ul>
                    </div>

                    <div class="flexslider-controls animate-this">
                       <ul class="flex-control-nav">
                          <li><img src="<?=base_url()?>resources/images/customer01.jpg" alt=""></li>
                          <li><img src="<?=base_url()?>resources/images/customer02.jpg" alt=""></li>
                          <li><img src="<?=base_url()?>resources/images/customer03.jpg" alt=""></li>
                       </ul>
                    </div>
                        
            </div>
        </div>
       </section> <!-- /testimonials -->