<div class="fullwidth full-page-header bg_mussel_1 coverbg bg-position-center">
    <div class="container">
        <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <h1 class="light-text maxwell">
                            About the project
                        </h1>
                    </div>
                </div>
        </div>
    </div>
</div>
<div class="container pb60">
    <div class="row">
        <div class="col-md-12">
                <h3>PROJECT TITLE</h3>

                <p>Suitability assessment and database development for enhanced mussel culture management using geospatial technologies</p>

                <h3>RATIONALE OF THE PROJECT</h3>
                <p>Green mussel is considered as an important and inexpensive source of protein
                    for people residing in the coastal areas (Nicholson, and Lam, 2005). Farming mussels
                    require little capital investment yet there has not been a significant increase in the
                    production in the last 10 years. This can be attributed to its low value, little market
                    demand, poor sanitary quality, occurrence of red tides, and unpredictable supply.
                </p>

                <p>To provide adequate response to the problem of low production in the mussel
                    industry, it is important to utilize possible sites where both hydrographic and biophysical
                    conditions favor mussel growth in the Philippines. The identification of suitable and
                    potential areas for mussel culture could provide impetus for expansion of mussel culture
                    leading to increased mussel production. The additional areas grown to mussels could
                    lead to a more sustainable mussel industry which can help in addressing national issues
                    like food security, livelihood and poverty alleviation.</p>

                <p>With the advent of recent geospatial technologies such as GIS, remote sensing
                    and GPS, quick and reliable information can be displayed visually for better
                    management of aquaculture areas. Geographic Information Systems (GIS) is a tool
                    used in studying the spatial characteristics of the marine environment for an effective
                    coastal management. The GIS capability to analyze such data could produce viable
                    outputs such as suitability maps which could provide useful information in the
                    management of mussel industry in the country.</p>
                
                <h3>OBJECTIVES</h3>
                <ul>
                    <li>
                        Identify potential sites for mussel culture based on established criteria
                    </li>
                    <li>
                        Development of web-based interactive mussel suitability map
                    </li>
                </ul>
            <div class="row">
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <img class="img-circle" alt="Bootstrap Image Preview" src="<?=base_url()?>resources/images/baylon.jpg" />
                            <h3>
                               Carlos C Baylon
                            </h3>
                            <h4>Project Leader</h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <img class="img-circle" alt="Bootstrap Image Preview" src="<?=base_url()?>resources/images/guzman.jpg" />
                            <h3>
                               Armi May. T. Guzman
                            </h3>
                            <h4>Co-Researcher<br/>UP Visayas </h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <img class="img-circle" alt="Bootstrap Image Preview" src="<?=base_url()?>resources/images/perez.jpg" />
                            <h3>
                               Gay Jane P. Perez 
                            </h3>
                            <h4>Co-Researchers<br/>UP Diliman</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
