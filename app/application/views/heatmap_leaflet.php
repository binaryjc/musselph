
    <script src="https://unpkg.com/leaflet@1.0.3/dist/leaflet.js"></script>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.3/dist/leaflet.css" />

    <link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-locatecontrol/v0.43.0/css/font-awesome.min.css' rel='stylesheet' />

    <div class="main-wrap">
            <input id="slide-sidebar" type="checkbox" role="button" />
            <label for="slide-sidebar" id="slide_toggle"><i class="fa fa-bars" aria-hidden="true"></i></label>
        <div class="sidebar">
            <div class="frmSearch">
                <img class="img-responsive img-align-center" src="<?=base_url()."resources/images/"?>logo_small.png">

                <div class="loader-wrapper"></div>
                <h3>Search</h3>
             <input type="text" id="search-box" class="form-control" placeholder="Search Location" />
            <div id="suggesstion-box"></div>
                <h3>Filter</h3>
                <form method="GET" action="<?=base_url()?>visitors/heatmap">
                    <label><input type="checkbox" checked name="filt_temp" id="filt_temp" /> Temperature</label> <br/>
                    <label><input type="checkbox" checked name="filt_chl" id="filt_chl"/> Chlorophyll a</label><br/>
                    <label><input type="checkbox" checked name="filt_sal" id="filt_sal" /> Salinity</label> <br/>
                    <label>Year</label>

                      <?php
                      // Sets the top option to be the current year. (IE. the option that is chosen by default).
                      $currently_selected = date('Y');
                      // Year to start available options at
                      $earliest_year = 2000;
                      // Set your latest year you want in the range, in this case we use PHP to just set it to the current year.
                      $latest_year = date('Y');
                      ?>
                    <select class="form-control" name="filt_year" id="filt_year">
                        <?php
                        /*foreach ( range( $latest_year, $earliest_year ) as $i ) {
                        // Prints the option with the next year in range.
                        print '<option value="'.$i.'"'.($i === $currently_selected ? ' selected="selected"' : '').'>'.$i.'</option>';
                        }*/
                        ?>
                        <option value="2012" selected>2012</option>
                        <option value="2013" >2013</option>
                        <option value="2014" >2014</option>
                        <option value="2015" >2015</option>
                        <option value="2016" >2016</option>
                    </select>
                    <label>Month</label>
                    <select class="form-control" name="filt_month" id="filt_month">
                        <option value="JAN">January</option>
                        <option value="FEB">February</option>
                        <option value="MAR">March</option>
                        <option value="APR">April</option>
                        <option value="MAY">May</option>
                        <option value="JUN">June</option>
                        <option value="JUL">July</option>
                        <option value="AUG">August</option>
                        <option value="SEP">September</option>
                        <option value="OCT">October</option>
                        <option value="NOV">November</option>
                        <option value="DEC" selected>December</option>
                    </select><br/>
                    <input type="hidden" value="" name="filt_lat" id="moved_lat">
                    <input type="hidden" value="" name="filt_long" id="moved_long">
                    <input type="button" id="submit_filter" name="" value="Submit" class="form-control">
                    <div id="dl_button_container" class="mtb15"></div>
                </form>
            </div>
        </div>
        <div id="main_heatmap">
            <div id='map' class="heatmap"></div>
            <div id="windyty"></div>
        </div>

        <div id="suitable_sites_filter_checkbox_container">
            <h5>Red Tide Frequency</h5>
            <label><input type="checkbox" id="red_site_check"><img class="img-responsive" src="<?=base_url()."resources/images/map_icons/"?>red.png"> Red Tide Sites</label>

            <h5>Grid Scale</h5>
            <span class="box" style="width:10px;height:10px;background:#1E7F43;display: inline-block;"></span> 1km per grid <br/>
            <span class="box" style="width:10px;height:10px;background:#F0CA4D;border-radius:50%;display: inline-block;"></span> 100km radius per circle
        </div>
    </div>

    <div id="heatmap_calculator_dashboard">
    </div>
    <div id="coordinates"></div>

    <input type="hidden" id="locate_self_lat" value="">
    <input type="hidden" id="locate_self_lng" value="">
    <div class="hidden">
        <p>Displayed parameters:<span id="state"></span></p>
        <p>Overlays: <span id="overlays"> <a>wind</a> <a>temp</a> <a>pressure</a> <a>clouds</a> <a>rh</a> <a>gust</a> <a>lclouds</a> <a>waves</a> <a>swell</a> <a>wwaves</a> <a>swellperiod</a> <a>sst</a> <a>sstanom</a> <a>currents</a> </span></p>

        <p>Levels: <span id="levels"><a>surface</a> <a>975h</a> <a>950h</a> <a>925h</a> <a>900h</a> <a>850h</a> <a>750h</a> <a>700h</a> <a>550h</a> <a>450h</a> <a>350h</a> <a>300h</a> <a>250h</a> <a>200h</a> <a>150h</a> </span></p>
        <br/>
    </div>

    <script type="text/javascript">

    //global functions
      var delay = (function(){
        var timer = 0;
        return function(callback, ms){
          clearTimeout (timer);
          timer = setTimeout(callback, ms);
        };
      })();
    //end global

        //http://localhost/musselph/app/visitors/heatmap_download?lat=10.722345344678637&lang=122.5605583190918&year=2015&month=12
        var map = L.map('map',{ zoomControl:true }).setView([10.722345344678637,122.5605583190918], 8);

        L.TileLayer.ShowAsDiv = L.TileLayer.extend({
          createTile: function (coords, done) {
              var tile = document.createElement('div');

              if (this.options.crossOrigin) {
                tile.crossOrigin = '';
              }

              tile.style.backgroundImage = "url('"+this.getTileUrl(coords)+"')";
              tile.style.visibility = 'inherit';

              return tile;
          },
        });

        L.tileLayer.showAsDiv = function(args) {
            return new L.TileLayer.ShowAsDiv(args);
        };

        var tiles = L.tileLayer.showAsDiv( 'http://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}.png', {
            maxZoom: 18,
            attribution: 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors',
        }).addTo(map);


        // control that shows state info on hover
        var info = L.control();

        info.onAdd = function (map) {
            this._div = L.DomUtil.create('div', 'info');
            this.update();
            return this._div;
        };

        info.update = function (props) {
            this._div.innerHTML = '' +  (props ?

                '<div class="data-content-props"><b>' + props.name + '</b><br />'
                + 'Chlorophyll a: '  + props.density +' (ug/L)<br />'
                + 'Temperature: '+ props.sst + ' (&#8451;)<br />'
                + 'Salinity: '+ props.salinity + ' (ppt)'
                +'</div>'
                : '');
        };

        info.addTo(map);
        
    //SETTINGS

        // get color depending on population density value
        function getColor(d) { //chlor_a - > yellow -> green dapat for chlor //http://www.december.com/html/spec/colorhslhex10.html
            return d > 12 ? '#08601b' :
                    d > 1  ? '#20ce45' :
                    d > 0  ? '#99f1a8' :
                                '#00000000';
        }
        function getColor_sst(d) { //sst - blue -> red dapat temperature
            return d > 27 ? '#d40b06' :
                    d > 22  ? '#fe6e6e' :
                    d < 22  ? '#febdb8' :
                                '#00000000';
        }

        function getColor_sal(d) { //salinity blue - upcoming data
            return d < 32 ? '#1e03bd' :
                    d > 27 ? '#1e03bd' :
                    d > 22  ? '#3480f0' :
                    d < 22  ? '#b2d3fc' :
                    d < 0  ? '#00000000' :
                                '#00000000';
        }



        //Blue sa salinity, r d sa temp kag green sa chloro guro pre

        function style(feature) {
            return {
                weight: 2,
                opacity: 1,
                color: 'transparent',
                dashArray: 0,
                fillOpacity: 0.5,
                fillColor: getColor(feature.properties.density)
            };
        }

        function style_sst(feature) {
            return {
                weight: 2,
                opacity: 1,
                color: 'transparent',
                dashArray: 0,
                fillOpacity: 0.5,
                fillColor: getColor_sst(feature.properties.sst)
            };
        }

        function style_sal(feature) {
            return {
                weight: 2,
                opacity: 1,
                color: 'transparent',
                dashArray: 0,
                fillOpacity: 0.5,
                fillColor: getColor_sal(feature.properties.salinity)
            };
        }

        function highlightFeature(e) {
            var layer = e.target;

            layer.setStyle({
                weight: 5,
                //color: '#666',
                dashArray: '',
                fillOpacity: 0.5
            });
            /*
            if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
                layer.bringToFront();
            }*/

            //info.update(layer.feature.properties);
        }

        var geojson;

        function resetHighlight(e) {
            //geojson.resetStyle(e.target);
            //info.update();
        }

        function zoomToFeature(e) {
            map.fitBounds(e.target.getBounds());
        }

        function onEachFeature(feature, layer) {
            layer.on({
                mouseover: highlightFeature,
                mouseout: resetHighlight,
                //click: zoomToFeature
            });
            
            //layer.bindPopup('<h1>'+feature.properties.f1+'</h1><p>name: '+feature.properties.f2+'</p>');
            layer.bindPopup( '<div class="data-content-props"><b>' + feature.properties.name + '</b><br />'
                + 'Chlorophyll a: '  + feature.properties.density +' (ug/L)<br />'
                + 'Temperature: '+ feature.properties.sst + ' (&#8451;)<br />'
                + 'Salinity: '+ feature.properties.salinity + ' (ppt)'
                +'</div>');
        }

        var get_chlora_data_with_location = function(current_lat,current_lng,withinkmradius,year,month){
            console.log('<?=base_url()?>admins/get_chlora_data_with_location/'+current_lat+'/'+current_lng+'/'+withinkmradius+'/'+year+'/'+month)
            $.getJSON('<?=base_url()?>admins/get_chlora_data_with_location/'+current_lat+'/'+current_lng+'/'+withinkmradius+'/'+year+'/'+month, function(statesData) {
                //console.log(statesData);
                //data is the JSON string

                geojson = L.geoJson(statesData, {
                    style: style,
                    onEachFeature: onEachFeature
                }).addTo(map);


            });
        }

        var get_sst_data_with_location = function(current_lat,current_lng,withinkmradius,year,month){
            console.log('<?=base_url()?>admins/get_sst_data_with_location/'+current_lat+'/'+current_lng+'/'+withinkmradius+'/'+year+'/'+month)
            $.getJSON('<?=base_url()?>admins/get_sst_data_with_location/'+current_lat+'/'+current_lng+'/'+withinkmradius+'/'+year+'/'+month, function(statesData2) {
                //console.log(statesData2);
                //data is the JSON string

                geojson2 = L.geoJson(statesData2, {
                    style: style_sst,
                    onEachFeature: onEachFeature
                }).addTo(map);


            });
        }

        var get_sal_data_with_location = function(current_lat,current_lng,withinkmradius,year,month){
            console.log('<?=base_url()?>admins/get_sal_data_with_location/'+current_lat+'/'+current_lng+'/'+withinkmradius+'/'+year+'/'+month)
            $.getJSON('<?=base_url()?>admins/get_sal_data_with_location/'+current_lat+'/'+current_lng+'/'+withinkmradius+'/'+year+'/'+month, function(statesData3) {
                //console.log(statesData2);
                //data is the JSON string

                geojson3 = L.geoJson(statesData3, {
                    style: style_sal,
                    onEachFeature: onEachFeature
                }).addTo(map);


            });
        }

    //SETTINGS END
      // placeholders for the L.marker and L.circle representing user's current position and accuracy
        var current_position, current_accuracy;

        function onLocationFound(e) {
          // if position defined, then remove the existing position marker and accuracy circle from the map
          if (current_position) {
              map.removeLayer(current_position);
              map.removeLayer(current_accuracy);
          }

            var radius = e.accuracy / 2;

            current_position = L.marker(e.latlng,{draggable: true}).addTo(map).bindPopup("You are within " + radius + " meters from this point. Drag this marker to set a new position").openPopup();
            current_position.on('dragend', ondragend);
            //ondragend();
            //
            current_accuracy = L.circle(e.latlng, radius).addTo(map);

            var m = current_position.getLatLng();
            $('#locate_self_lat').val(m.lat);
            $('#locate_self_lng').val(m.lng);

            var year = $('#filt_year').val();
            var month = $('#filt_month').val();

            if ($('#filt_chl').prop('checked')){
                get_chlora_data_with_location(m.lat,m.lng,100,year,month);
            }
            if ($('#filt_temp').prop('checked')){
                setTimeout(function(){
                    get_sst_data_with_location(m.lat,m.lng,100,year,month);
                },1000); //delay for a second so that color is overlayed
            }
            if ($('#filt_sal').prop('checked')){
                setTimeout(function(){
                    get_sal_data_with_location(m.lat,m.lng,100,year,month);
                },2000); //delay for a second so that color is overlayed
            }

            generate_dl_button(m.lat,m.lng,year,month);

            /*
            L.easyPrint({
                    title: 'My awesome print button',
                    position: 'topright',
                    sizeModes: ['A4Portrait', 'A4Landscape']
                }).addTo(map);*/

        }

        //if user is not located via browser geolocation
        function onLocationError(e) {
            initiate_marker();
        }

        function initiate_marker(){
            //11.706030770773827,122.87658691406249

            current_position = L.marker([11.706030770773827,122.87658691406249],{draggable: true}).addTo(map).bindPopup("Move the marker to desired position").openPopup();
            current_position.on('dragend', ondragend);

            var m = current_position.getLatLng();
            $('#locate_self_lat').val(m.lat);
            $('#locate_self_lng').val(m.lng);

            var year = $('#filt_year').val();
            var month = $('#filt_month').val();

            if ($('#filt_chl').prop('checked')){
                get_chlora_data_with_location(m.lat,m.lng,100,year,month);
            }
            if ($('#filt_temp').prop('checked')){
                setTimeout(function(){
                    get_sst_data_with_location(m.lat,m.lng,100,year,month);
                },1000); //delay for a second so that color is overlayed
            }
            if ($('#filt_sal').prop('checked')){
                setTimeout(function(){
                    get_sal_data_with_location(m.lat,m.lng,100,year,month);
                },2000); //delay for a second so that color is overlayed
            }


            generate_dl_button(m.lat,m.lng,year,month);
        }

        function ondragend() {
            //var coordinates = document.getElementById('coordinates');
            var m = current_position.getLatLng();
           // coordinates.innerHTML = 'Latitude: ' + m.lat + '<br />Longitude: ' + m.lng;
            $('#locate_self_lat').val(m.lat);
            $('#locate_self_lng').val(m.lng);

            $('#moved_lat').val(m.lat);
            $('#moved_long').val(m.lng);

            var year = $('#filt_year').val();
            var month = $('#filt_month').val();
            /*
            if ($('#filt_chl').prop('checked')){
                geojson.clearLayers();
                get_chlora_data_with_location(m.lat,m.lng,100,year,month);
            }
            if ($('#filt_temp').prop('checked')){
                geojson2.clearLayers();
                setTimeout(function(){
                    get_sst_data_with_location(m.lat,m.lng,100,year,month);
                },1000); //delay for a second so that color is overlayed
            }*/
        }

        map.on('locationfound', onLocationFound);
        map.on('locationerror', onLocationError);

        // wrap map.locate in a function
        function locate() {
          map.locate({setView: true, maxZoom: 8});
        }

        locate();

        //unused function - old version for relocating marker from search
        function relocateMarker(lat,lon){
                geojson.clearLayers();
                geojson2.clearLayers();
                geojson3.clearLayers();
                get_chlora_data_with_location(lat,lon,100);
                setTimeout(function(){
                    get_sst_data_with_location(lat,lon,100);
                },1000);
                setTimeout(function(){
                    get_sal_data_with_location(lat,lon,100);
                },2000);
        }

            $("#search-box").keyup(function(){
                if($( "#search-box").val().length >= 3){
                    delay(function(){
                        $.ajax({
                        type: "POST",
                        url: "<?=base_url()?>visitors/search_osm_locations",
                        data:'keyword='+$("#search-box").val(),
                        dataType:'json',
                        beforeSend: function(){
                           // $("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
                            console.log('before sending request');
                            spinnerLoad();
                        },
                        success: function(data){
                            console.log('request successfull');
                            $("#suggesstion-box").empty();
                            //$("#suggesstion-box").show();
                            //$("#search-box").css("background","#FFF");
                            //console.log(data);
                            $.each(data, function(key,entry_data) {
                                $("#suggesstion-box").append(
                                    '<li class="search_result_entry" data-lat="'+entry_data['lat']+'"data-lon="'+entry_data['lon']+'" >'+
                                    entry_data['display_name']+
                                    '</li>'
                                );
                            });
                            spinnerUnload();
                            $(".search_result_entry").click(function(event) {
                                var lat = $(this).attr('data-lat');
                                var lon = $(this).attr('data-lon');
                                console.log(lat);
                                console.log(lon);

                                var year = $('#filt_year').val();console.log(year);
                                var month = $('#filt_month').val();console.log(month);

                                map.removeLayer(current_position);
                                current_position = L.marker([lat,lon],{draggable: true}).addTo(map).bindPopup("You have set this as a new position").openPopup();
                                current_position.on('dragend', ondragend);

                                $('#moved_lat').val(lat);
                                $('#moved_long').val(lon);

                                if ($('#filt_chl').prop('checked')){
                                    geojson.clearLayers();
                                    get_chlora_data_with_location(m.lat,m.lng,100,year,month);
                                }
                                if ($('#filt_temp').prop('checked')){
                                    geojson2.clearLayers();
                                    setTimeout(function(){
                                        get_sst_data_with_location(m.lat,m.lng,100,year,month);
                                    },1000); //delay for a second so that color is overlayed

                                    geojson3.clearLayers();
                                    setTimeout(function(){
                                        get_sal_data_with_location(m.lat,m.lng,100,year,month);
                                    },1000); //delay for a second so that color is overlayed
                                }

                                generate_dl_button(m.lat,m.lng,year,month);

                            });
                        },
                        error: function(e){
                            console.log(e.message);
                        }
                        });
                    }, 1000 );
                }
            });

            $("#submit_filter").click(function(){
                    spinnerLoad();
                    console.log('click sulod');
                    var submitedlat = $('#moved_lat').val();
                    var submitedlng = $('#moved_long').val();

                    var year = $('#filt_year').val();
                    var month = $('#filt_month').val();
                    //if(geojson){
                    if (typeof geojson !== 'undefined') {
                        geojson.clearLayers();
                        console.log('removing old layer');
                    }
                    //if(geojson2){
                    if (typeof geojson2 !== 'undefined') {
                        geojson2.clearLayers();
                        console.log('removing old layer2');
                    }
                    //if(geojson3){
                    if (typeof geojson3 !== 'undefined') {
                        geojson3.clearLayers();
                        console.log('removing old layer3');
                    }

                    if ($('#filt_chl').prop('checked')){
                        get_chlora_data_with_location(submitedlat,submitedlng,100,year,month);
                    }
                    if ($('#filt_temp').prop('checked')){
                        setTimeout(function(){
                            get_sst_data_with_location(submitedlat,submitedlng,100,year,month);
                        },1000); //delay for a second so that color is overlayed
                        spinnerUnload();
                    }
                    if ($('#filt_sal').prop('checked')){
                        setTimeout(function(){
                            get_sal_data_with_location(submitedlat,submitedlng,100,year,month);
                        },3000); //delay for a second so that color is overlayed
                        spinnerUnload();
                    }

                    generate_dl_button(submitedlat,submitedlng,year,month);

            });

        function generate_dl_button(lat,long,year,month){
            ////http://localhost/musselph/app/visitors/heatmap_download?lat=10.722345344678637&long=122.5605583190918&year=2015&month=12
            $('#dl_button_container').html("");
            $('#dl_button_container').append(
                '<a class="btn btn-success btn-block" target="_blank" href="<?=base_url()?>visitors/heatmap_download?lat='+lat+'&long='+long+'&year='+year+'&month='+month+'">Download This</a>'
            );
            $('.notif-wrapper .general-notification').fadeIn(300);
        }


        //RED TIDE
        //

        var red_layer_array = [];
        var getRedTides = function(){
            <?php
                $getAllRed = base_url()."mapbox/getAllReds/";
            ?>
            console.log("<?= $getAllRed; ?>");
            $.getJSON("<?= $getAllRed; ?>", function (red_data){

                $.each(red_data, function(id,entry_data){

                    //var lat = parseFloat(res[0], 10);
                    //var lng = parseFloat(res[1], 10);
                    var latlng = L.latLng(entry_data['lat'], entry_data['lng']);
                    var name = entry_data['location'];

                    var content = '<h3 class="text-bold pop-title">'+entry_data['location']+'</h3>'; // disabled content and breaks

                    //parameters
                    var red_tide = entry_data['red_sum'];
                    if (red_tide > 0){
                        content += '<img alt="" class="paramerter_icon" src="<?php echo base_url();?>resources/images/parameter_icons/red_tide.png"/>&nbsp;<span class="text-bold">Red Tide Frequency:</span>&nbsp;'+red_tide+'<br/>';
                    }

                    //var marky = L.marker(latlng, {icon: redIcon}).bindPopup(content);
                    var marky = L.circle(latlng,entry_data['red_sum']*50,{color: 'red',fillColor: 'red',fillOpacity: 0.5,weight:1}).bindPopup(content);

                    red_layer_array.push(marky);


                });

                var red_layer = L.layerGroup(red_layer_array);

                document.getElementById("red_site_check").onclick = function() {
                    if (this.checked) { map.addLayer(red_layer);} else {map.removeLayer(red_layer);}
                }

            });
        }
        getRedTides();


    var spinnerLoad = function(){
      $('.loader-wrapper').text("");
      $('.loader-wrapper').append(
        '<div class="general-notification trans-dark align-center">'+
            '<img src="<?=base_url()?>resources/images/loader.gif">'+
        '</div>'
        );
      $('.notif-wrapper .general-notification').fadeIn(300);
    };

    var spinnerUnload= function(){
      $('.loader-wrapper').fadeOut(3000, function() {$(this).empty().show();});
    };
    </script>

