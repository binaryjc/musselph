<div class="container">
    <div class="row">
		<div class="well">
            <h1 class="text-center mtb20">Suitability Calculator</h1>
            <h2 class="text-center"><small>Want to check if your area is suitable? Answer the questions below</small></h2>
            <div class="text-center mb15">
                <a href="#" title="Click here to show instructions" data-toggle="modal" data-target="#instructions">How To Use <i class="fa fa-question-circle" aria-hidden="true"></i></a>
                <a href="#" title="Click here to show Optimum parameters for green mussel" data-toggle="modal" data-target="#optimum">Optimum parameters for green mussel</a>
            </div>
        </div>
	</div>
    <div class="row">
        <div class="well">  
            <h3 class="text-center">PRIMARY</h3>
            <div class="row">
            
                <div class="col-xs-12 col-sm-6">
                    <div class="list-group">
                      <a href="#" class="list-group-item">
                            <div class="media col-md-3 text-center">
                                <figure class="">
                                    <img class="media-object img-rounded img-responsive img-align-center"  src="<?=base_url().'resources/images/calculator_icons/enclosed.png'?>" alt="Corals" >
                                </figure>
                                <h2><small>Yes/No</small></h2>
                                <input data-toggle="toggle" data-size="normal"data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger" type="checkbox" id="data_enclosed" name="data_enclosed">
                            </div>
                            <div class="col-md-9 ptb20">
                                <h4 class="list-group-item-heading">Enclosed areas</h4>
                                <p class="list-group-item-text">
                                    Areas such as bays and coves are recommended as ideal culture sites due to the limited wave action it receives. Intense waves destroy the culture set-up and commonly leads to high effort and a lesser yield. 
                                    <br/><br/>
                                    Is your area enclosed?
                                </p>
                            </div>
                      </a>
                      <a href="#" class="list-group-item">
                            <div class="media col-md-3 text-center">
                                <figure class="">
                                    <img class="media-object img-rounded img-responsive img-align-center"  src="<?=base_url().'resources/images/calculator_icons/sandy.png'?>" alt="Corals" >
                                </figure>
                                <h2><small>Yes/No</small></h2>
                                <input data-toggle="toggle" data-size="normal"data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger" type="checkbox" id="data_sandy" name="data_sandy">
                            </div>
                            <div class="col-md-9 ptb20">
                                <h4 class="list-group-item-heading">Sandy loam bottom substrate</h4>
                                <p class="list-group-item-text">
                                    Mussels favor growth in murky waters which contain high levels of nutrients which are commonly found in sandy loam substrates.  
                                    <br/><br/>Does your area have sandy loam sediment floor?
                                     </p>
                            </div>
                      </a>
                      <a href="#" class="list-group-item">
                            <div class="media col-md-3 text-center">
                                <figure class="">
                                    <img class="media-object img-rounded img-responsive img-align-center"  src="<?=base_url().'resources/images/calculator_icons/depth.png'?>" alt="Corals" >
                                </figure>
                                <h2><small>Yes/No</small></h2>
                                <input data-toggle="toggle" data-size="normal"data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger" type="checkbox" id="data_depth" name="data_depth">
                            </div>
                            <div class="col-md-9 ptb20">
                                <h4 class="list-group-item-heading">Depth below 3 meters</h4>
                                <p class="list-group-item-text">
                                   Optimum depth requirement for growing mussels is 3-7 meters considering the lowest low tide of the area. The animal could not survive long exposures to air.
                                    <br/><br/>
                                    Is the depth of your area 3 meters on the lowest low tide?
                                </p>
                            </div>
                      </a>

                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="list-group">
                        
                      <a href="#" class="list-group-item">
                            <div class="media col-md-3 text-center">
                                <figure class="">
                                    <img class="media-object img-rounded img-responsive img-align-center"  src="<?=base_url().'resources/images/calculator_icons/salinity.png'?>" alt="Corals" >
                                </figure>
                                <h2><small>Yes/No</small></h2>
                                <input data-toggle="toggle" data-size="normal"data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger" type="checkbox" id="data_salinity" name="data_salinity">
                            </div>
                            <div class="col-md-9 ptb20">
                                <h4 class="list-group-item-heading">Salinity within optimum range</h4>
                                <p class="list-group-item-text">
                                    This is the concentration of dissolved salts in water and is usually expressed in parts per thousands (ppt). Commonly, salinity is known as the saltiness of water. Optimum salinity level for green mussel is 27-32 ppt. Values lower or higher than this may be tolerable but does not favor ideal mussel growth.
                                    <br/><br/>
                                    Is your area within the optimum salinity for green mussel?
                                </p>
                            </div>
                      </a>
                      <a href="#" class="list-group-item">
                            <div class="media col-md-3 text-center">
                                <figure class="">
                                    <img class="media-object img-rounded img-responsive img-align-center"  src="<?=base_url().'resources/images/calculator_icons/avail.png'?>" alt="Corals" >
                                </figure>
                                <h2><small>Yes/No</small></h2>
                                <input data-toggle="toggle" data-size="normal"data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger" type="checkbox" id="data_avail" name="data_avail">
                            </div>
                            <div class="col-md-9 ptb20">
                                <h4 class="list-group-item-heading">Available source of mussel spats</h4>
                                <p class="list-group-item-text">
                                The handling, transportation and production of spats should be in compliance to FAO 209 of RA 10654. Spats should come from authorized hatcheries to eliminate and deter transfer of dinoflagellates that might carry and cause red tide in pathogen free areas. 
                                <br/><br/>
                                    Is there a nearby supplier of mussel spats in your area?
                                     </p>
                            </div>
                      </a>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="well"> 
            <h3 class="text-center">SOCIAL</h3>
            <div class="row">
            
                <div class="col-xs-12 col-sm-6">
                    <div class="list-group">
                      <a href="#" class="list-group-item">
                            <div class="media col-md-3 text-center">
                                <figure class="">
                                    <img class="media-object img-rounded img-responsive img-align-center"  src="<?=base_url().'resources/images/calculator_icons/coral.png'?>" alt="Corals" >
                                </figure>
                                <h2><small>Yes/No</small></h2>
                                <input data-toggle="toggle" data-size="normal"data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger" type="checkbox" id="data_coral" name="data_coral">
                            </div>
                            <div class="col-md-9 ptb20">
                                <h4 class="list-group-item-heading">Coral Reef Area</h4>
                                <p class="list-group-item-text">
                                    Corals are the defining species on most marine ecosystems. They serve as home to thousands of marine species and commonly feed on zooxanthellae. They are usually found in shallow areas at a depth of less than 150 feet depending on the type of colonies being formed.
                                    <br/><br/>
                                    Are there corals in your area?
                                </p>
                            </div>
                      </a>
                      <a href="#" class="list-group-item">
                            <div class="media col-md-3 text-center">
                                <figure class="">
                                    <img class="media-object img-rounded img-responsive img-align-center"  src="<?=base_url().'resources/images/calculator_icons/ship.png'?>" alt="Corals" >
                                </figure>
                                <h2><small>Yes/No</small></h2>
                                <input data-toggle="toggle" data-size="normal"data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger" type="checkbox" id="data_ship" name="data_ship">
                            </div>
                            <div class="col-md-9 ptb20">
                                <h4 class="list-group-item-heading">Navigational channel for ships/ boats</h4>
                                <p class="list-group-item-text">
                                    This includes tourism dock-offs, commercial and private ports. There should be less activity in the culture area as to not obstruct navigational routes of ships and boats. Also, this is to minimize the chance of oil spill contamination and the presence of alien species brought by ballast ships. 
                                    <br/><br/>Is there an existing port in your area?
                                     </p>
                            </div>
                      </a>
                      <a href="#" class="list-group-item">
                            <div class="media col-md-3 text-center">
                                <figure class="">
                                    <img class="media-object img-rounded img-responsive img-align-center"  src="<?=base_url().'resources/images/calculator_icons/road.png'?>" alt="Corals" >
                                </figure>
                                <h2><small>Yes/No</small></h2>
                                <input data-toggle="toggle" data-size="normal"data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger" type="checkbox" id="data_road" name="data_road">
                            </div>
                            <div class="col-md-9 ptb20">
                                <h4 class="list-group-item-heading">With concrete market roads</h4>
                                <p class="list-group-item-text">
                                    Accessibility of the area should be put into consideration. For assurance of income return and yield, target markets should be easily reached. 
                                </p>
                                    <br/><br/>Is the area accessible or is there a landing site nearby?
                            </div>
                      </a>

                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="list-group">
                      
                      <a href="#" class="list-group-item">
                            <div class="media col-md-3 text-center">
                                <figure class="">
                                    <img class="media-object img-rounded img-responsive img-align-center"  src="<?=base_url().'resources/images/calculator_icons/tourism.png'?>" alt="Corals" >
                                </figure>
                                <h2><small>Yes/No</small></h2>
                                <input data-toggle="toggle" data-size="normal"data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger" type="checkbox" id="data_tourism" name="data_tourism">
                            </div>
                            <div class="col-md-9 ptb20">
                                <h4 class="list-group-item-heading">Tourism Area</h4>
                                <p class="list-group-item-text">
                                    Areas which have higher values for tourism should not be altered. These areas share high economic stability which provides a lot of opportunities and livelihood to the people. 
                                    <br/><br/>Is the area intended for tourism activities?
                                     </p>
                            </div>
                      </a>
                      <a href="#" class="list-group-item">
                            <div class="media col-md-3 text-center">
                                <figure class="">
                                    <img class="media-object img-rounded img-responsive img-align-center"  src="<?=base_url().'resources/images/calculator_icons/pollute.png'?>" alt="Corals" >
                                </figure>
                                <h2><small>Yes/No</small></h2>
                                <input data-toggle="toggle" data-size="normal"data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger" type="checkbox" id="data_pollute" name="data_pollute">
                            </div>
                            <div class="col-md-9 ptb20">
                                <h4 class="list-group-item-heading">Highly polluted (Agriculture, Domestic, Industrial)</h4>
                                <p class="list-group-item-text">
                                    Highly populated areas eliminate high loads of waste in the water which may contaminate the animal. These areas have very poor water quality than can affect the growth and survival of mussels. 
                                    <br/><br/>Is the area near households, agricultural lands and industrial buildings?
                                     </p>
                            </div>
                      </a>
                      <a href="#" class="list-group-item">
                            <div class="media col-md-3 text-center">
                                <figure class="">
                                    <img class="media-object img-rounded img-responsive img-align-center"  src="<?=base_url().'resources/images/calculator_icons/tide.png'?>" alt="Corals" >
                                </figure>
                                <h2><small>Yes/No</small></h2>
                                <input data-toggle="toggle" data-size="normal"data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger" type="checkbox" id="data_tide" name="data_tide">
                            </div>
                            <div class="col-md-9 ptb20">
                                <h4 class="list-group-item-heading">History of red tide</h4>
                                <p class="list-group-item-text">
                                    Red tide or commonly called as harmful algal blooms (HAB’s) is a phenomenon wherein marine, estuarine or freshwater algae accumulate in the water column and bloom. Toxic levels of red tide pose concerns affecting the health of people and marine ecosystems.  
                                    <br/><br/>
                                    Is there a history of red tide occurrence in your area?
                                </p>
                            </div>
                      </a>

                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="well">         
            <div id="results_container">
                <div><h3>Results:</h3></div>
                <div id="result_text"></div>
                <div id="result_summary">
                      <div><h3>Summary:</h3></div>
                      <table class="table table-striped">
                        <thead>
                          <tr>
                            <th>Indicator</th>
                            <th>Answer</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Coral Reef Area</td> 
                            <td><span id="r_coral"></span></td>
                          </tr>
                          <tr>
                            <td>Depth below 3 meters</td>
                            <td><span id="r_depth"></span></td>
                          </tr>
                          <tr>
                            <td>Salinity</td>
                            <td><span id="r_salinity"></span></td>
                          </tr>
                          <tr>
                            <td>History of red tide</td>
                            <td><span id="r_tide"></span></td>
                          </tr>
                          <tr>
                            <td>Enclosed areas</td>
                            <td><span id="r_enclosed"></span></td>
                          </tr>
                          <tr>
                            <td>Available source of mussel spats</td>
                            <td><span id="r_avail"></span></td>
                          </tr>
                          <tr>
                            <td>With concrete market roads</td>
                            <td><span id="r_road"></span></td>
                          </tr>
                          <tr>
                            <td>Navigational channel for ships/ boats</td>
                            <td><span id="r_ship"></span></td>
                          </tr>
                          <tr>
                            <td>Tourism Area</td>
                            <td><span id="r_tourism"></span></td>
                          </tr>
                          <tr>
                            <td>Highly polluted (Agriculture, Domestic, Industrial)</td>
                            <td><span id="r_pollute"></span></td>
                          </tr>
                          <tr>
                            <td>Sandy loam bottom substrate</td>
                            <td><span id="r_sandy"></span></td>
                          </tr>
                        </tbody>
                      </table>
                </div>
                <a href="#" class="btn btn-default"> Download (Soon)</a>
            </div>
        </div>
    </div>
</div>

<script>

$('input[type=checkbox]').bootstrapToggle('off');
$("input[type=checkbox]").change(function(){
  //alert('checkbox changed');
  recalculate();
});    
function recalculate(){
    if($('#data_coral').is(":checked")){ $('#r_coral').html('Yes'); }else{ $('#r_coral').html('No'); }
    if($('#data_depth').is(":checked")){ $('#r_depth').html('Yes'); }else{ $('#r_depth').html('No'); }
    if($('#data_salinity').is(":checked")){ $('#r_salinity').html('Yes'); }else{ $('#r_salinity').html('No'); }
    if($('#data_tide').is(":checked")){ $('#r_tide').html('Yes'); }else{ $('#r_tide').html('No'); }
    if($('#data_enclosed').is(":checked")){ $('#r_enclosed').html('Yes'); }else{ $('#r_enclosed').html('No'); }
    if($('#data_avail').is(":checked")){ $('#r_avail').html('Yes'); }else{ $('#r_avail').html('No'); }
    if($('#data_road').is(":checked")){ $('#r_road').html('Yes'); }else{ $('#r_road').html('No'); }
    if($('#data_ship').is(":checked")){ $('#r_ship').html('Yes'); }else{ $('#r_ship').html('No'); }
    if($('#data_tourism').is(":checked")){ $('#r_tourism').html('Yes'); }else{ $('#r_tourism').html('No'); }
    if($('#data_pollute').is(":checked")){ $('#r_pollute').html('Yes'); }else{ $('#r_pollute').html('No'); }
    if($('#data_sandy').is(":checked")){ $('#r_sandy').html('Yes'); }else{ $('#r_sandy').html('No'); }

    /*
    if($('#data_salinity').is(":checked") && $('#data_enclosed').is(":checked") && $('#data_avail').is(":checked") && $('#data_road').is(":checked") && $('#data_sandy').is(":checked")){
        $('#result_text').html('<span class="result_success"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Suitable</span>');
    }else{
        $('#result_text').html('<span class="result_error"><i class="fa fa-times-circle-o" aria-hidden="true"></i> Not Suitable</span>');
    }
    */
   
    if( //YES
        $('#data_salinity').is(":checked") && 
        $('#data_enclosed').is(":checked") && 
        $('#data_avail').is(":checked") && //source of spats
        $('#data_road').is(":checked") && 
        $('#data_sandy').is(":checked")){
        $('#result_text').html('<span class="result_success"><i class="fa fa-check-circle-o" aria-hidden="true"></i> Suitable</span>');
    }else if( //YES
        $('#data_salinity').is(":checked") && 
        $('#data_tide').is(":checked") && 
        $('#data_enclosed').is(":checked") && 
        $('#data_ship').is(":checked") && // navi channel
        $('#data_sandy').is(":checked")){
    }else if( //YES
        $('#data_salinity').is(":checked") && 
        $('#data_enclosed').is(":checked") && 
        $('#data_avail').is(":checked") && //source of spats
        $('#data_sandy').is(":checked")){
    }else if( //YES
        $('#data_salinity').is(":checked") && 
        $('#data_tide').is(":checked") && 
        $('#data_enclosed').is(":checked") && 
        $('#data_avail').is(":checked") && //source of spats
        $('#data_road').is(":checked") && 
        $('#data_ship').is(":checked") && // navi channel
        $('#data_sandy').is(":checked")){
    }else{
        $('#result_text').html('<span class="result_error"><i class="fa fa-times-circle-o" aria-hidden="true"></i> Not Suitable</span>');
    }
    
    if($('#data_coral').is(":checked") || $('#data_depth').is(":checked") || $('#data_tourism').is(":checked") || $('#data_pollute').is(":checked")){
        $('#result_text').html('<span class="result_error"><i class="fa fa-times-circle-o" aria-hidden="true"></i> Not Suitable</span>');
    }

}
</script>
<?php 
/*
Coral Reef Area (N)
Depth below 3 meters (N)
Salinity (Y)
History of red tide (N)
Enclosed areas (Y)
Available source of mussel spats (Y)
With concrete market roads (Y)
Navigational channel for ships/ boats (N) 
Tourism Area (N)
Highly polluted (Agriculture, Domestic, Industrial) (N)
Sandy loam bottom substrate (Y)
*/
?>