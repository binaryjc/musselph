   </div> <!-- /main-content-wrap -->


   <!-- footer
	================================================== -->
	<footer id="main-footer">

   	<div class="footer-social-wrap">  
   		<div class="row">
					
	         <ul class="footer-social-list">
	            <li><a href="#">
	             	<i class="fa fa-facebook-square"></i>
	            </a></li>
	            <li><a href="#">
	              	<i class="fa fa-twitter"></i>
	            </a></li>
              <li><a href="#">
	              	<i class="fa fa-google-plus"></i>
	            </a></li>
	            <li><a href="#">
	              	<i class="fa fa-pinterest"></i>
	            </a></li>
	            <li><a href="#">
	              	<i class="fa fa-instagram"></i>
	            </a></li>
	            <li><a href="#">
	              	<i class="fa fa-dribbble"></i>
	            </a></li>
	         </ul>
		         
			</div> 
   	</div> <!-- /footer-social-wrap -->

	   <div class="footer-info-wrap">
	   	<div class="row footer-info">

		  	<div class="col-four tab-full">
		  			<h4><i class="icon-location-map-1"></i> Where to Find Us</h4>

		  			<p>
		         1600 Amphitheatre Parkway<br>
		         Mountain View, CA<br>
		         94043 US
		         </p>
		  	</div>

		   	<div class="col-four tab-full">
	   			<h4><i class="icon-phone-incoming"></i> Get In Touch</h4>

	   			<p>musselphilippines.com<br>
				   	info.musselphilippines@gmail.com <br>
				   	Phone: (+63) 555 1212			     
				   </p>
	   		</div>

	   		<div class="col-four tab-full">
	   			<h4><i class="icon-organization-hierarchy-3"></i> Website Links</h4>

				   <ul class="footer-link-list">
				   	<li><a href="#">Company Journal</a></li>
				   	<li><a href="#">Product Support</a></li>
				   	<li><a href="#">Privacy Policy</a></li>
				   </ul>
	   		</div>
		   		
		</div>
	   </div> <!-- /footer-info-wrap -->
	   	
	   <div class="footer-bottom"> 

	   	<div class="copyright">
		     	<span>© Copyright MusselPhilippines 2017.</span> 
		     	<span>University of the Philippines</span>	         	
		   </div>  		
   	</div>
	   	
   </footer>   

   <div id="go-top">
		<a class="smoothscroll" title="Back to Top" href="#top"><i class="fa fa-long-arrow-up"></i></a>
	</div>

   <!-- preloader
   ================================================== -->
   <div id="preloader"> 
    	<div id="loader"></div>
   </div> 

   <!-- Java Script
   ================================================== --> 
   <script src="<?=base_url()?>resources/js/jquery-2.1.3.min.js"></script>
   <script src='<?=base_url()?>resources/js/bootstrap.min.js'></script>
   <script src="<?=base_url()?>resources/js/plugins.js"></script>
   <script src="<?=base_url()?>resources/js/main.js"></script>

</body>

</html>