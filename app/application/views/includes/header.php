<!DOCTYPE html>
<html>
<head>
    <meta charset=utf-8 />
    <title>Mussel Philippines</title>
    <meta name='viewport' content='initial-scale=1,maximum-scale=1,user-scalable=no' />
    <!-- <link href='https://api.mapbox.com/mapbox.js/v2.3.0/mapbox.css' rel='stylesheet' /> -->
    <script src="<?= base_url(); ?>resources/js/jquery-2.1.3.js"></script>

    <script src='<?=base_url()?>resources/js/bootstrap.min.js'></script>
    <!--<link rel="stylesheet" href="<?=base_url()?>resources/css/zoom.css" />-->
    <link rel="stylesheet" href="<?=base_url()?>resources/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>resources/css/styles.css" />
    <link rel="stylesheet" href="<?=base_url()?>resources/font-awesome/css/font-awesome.min.css" />
    <link rel="icon" type="image/png" href="<?=base_url()?>resources/images/favicon.png">
    <?php if(isset($gallery_css)){ ?>
        <link rel="stylesheet" href="<?=base_url()?>resources/css/gallery.css" />
    <?php }?>
    <?php if($page_index == 'calculator'){ ?>
        <link href="<?=base_url()?>resources/css/bootstrap-toggle.min.css" rel="stylesheet">
        <script src="<?=base_url()?>resources/js/bootstrap-toggle.min.js"></script>
    <?php }?>

</head>
<body class="<?php if(isset($page_index)){echo $page_index;} ?>" >

<header id="main-header" data-height-onload="80" data-height-loaded="true" style="top: 0px;" class="">
            <div class="container clearfix et_menu_container">
                            <div class="logo_container">
                    <span class="logo_helper"></span>
                    <a href="http://musselphilippines.com/">
                        <img src="http://musselphilippines.com/wp-content/uploads/2017/03/mussel_ph_logo_vintage.png" alt="Mussel Philippines" id="logo" data-height-percentage="54" data-actual-width="500" data-actual-height="500">
                    </a>
                </div>
                <div id="et-top-navigation" data-height="66" data-fixed-height="40" style="padding-left: 73px;">
                                            <nav id="top-menu-nav">
                        <ul id="top-menu" class="nav"><li id="menu-item-164" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-46 current_page_item menu-item-164"><a href="http://musselphilippines.com/">Home</a></li>
<li id="menu-item-425" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-425"><a href="#">Products (Maps)</a>
<ul class="sub-menu">
    <li id="menu-item-165" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-165"><a href="http://musselphilippines.com/app/visitors/sites">Suitable Sites</a></li>
    <li id="menu-item-166" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-166"><a href="http://musselphilippines.com/app/visitors/heatmap">Interactive Database</a></li>
    <li id="menu-item-167" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-167"><a href="http://musselphilippines.com/app/visitors/suitability_calculator">Suitability Calculator</a></li>
</ul>
</li>
<li id="menu-item-426" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-426"><a href="http://musselphilippines.com/list-of-personels/">Contact List</a></li>
<li id="menu-item-427" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-427"><a href="http://musselphilippines.com/contact-us/">Contact Us</a></li>
</ul>                       </nav>
                    
                    
                    
                    
                    <div id="et_mobile_nav_menu">
                <div class="mobile_nav closed">
                    <span class="select_page">Select Page</span>
                    <span class="mobile_menu_bar mobile_menu_bar_toggle"></span>
                <ul id="mobile_menu" class="et_mobile_menu"><li id="menu-item-164" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-46 current_page_item menu-item-164 et_first_mobile_item"><a href="http://musselphilippines.com/">Home</a></li>
<li id="menu-item-425" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-425"><a href="http://#">Products (Maps)</a>
<ul class="sub-menu">
    <li id="menu-item-165" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-165"><a href="http://musselphilippines.com/app/visitors/sites">Suitable Sites</a></li>
    <li id="menu-item-166" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-166"><a href="http://musselphilippines.com/app/visitors/heatmap">Interactive Database</a></li>
    <li id="menu-item-167" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-167"><a href="http://musselphilippines.com/app/visitors/suitability_calculator">Suitability Calculator</a></li>
</ul>
</li>
<li id="menu-item-426" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-426"><a href="http://musselphilippines.com/list-of-personels/">Contact List</a></li>
<li id="menu-item-427" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-427"><a href="http://musselphilippines.com/contact-us/">Contact Us</a></li>
</ul></div>
            </div>              </div> <!-- #et-top-navigation -->
            </div> <!-- .container -->

        </header>
<div id="main-wrap">
    