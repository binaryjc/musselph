<!DOCTYPE html>
<html>
<head>
    <meta charset=utf-8 />
    <title>Mussel Philippines</title>
    <meta name='viewport' content='initial-scale=1,maximum-scale=1,user-scalable=no' />
    <!-- <link href='https://api.mapbox.com/mapbox.js/v2.3.0/mapbox.css' rel='stylesheet' /> -->
    <script src="<?= base_url(); ?>resources/js/jquery-2.1.3.js"></script>

    <script src='<?=base_url()?>resources/js/bootstrap.min.js'></script>
    <!--<link rel="stylesheet" href="<?=base_url()?>resources/css/zoom.css" />-->
    <link rel="stylesheet" href="<?=base_url()?>resources/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>resources/css/styles.css" />
    <link rel="stylesheet" href="<?=base_url()?>resources/font-awesome/css/font-awesome.min.css" />
    <link rel="icon" type="image/png" href="<?=base_url()?>resources/images/favicon.png">


</head>
<body class="<?php if(isset($page_index)){echo $page_index;} ?>" >

<div id="main-wrap">
    