<!DOCTYPE html>
<!--[if IE 8 ]><html class="no-js oldie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="no-js oldie ie9" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>

   <!--- basic page needs
   ================================================== -->
   <meta charset="utf-8">
	<title>Mussel Philippines</title>
	<meta name="description" content="">  
	<meta name="author" content="">

   <!-- mobile specific metas
   ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

 	<!-- Main JS with Bootstrap
   ================================================== -->

 	<!-- CSS
   ================================================== -->
   <link rel="stylesheet" href="<?=base_url()?>resources/css/bootstrap.min.css" />
   <link rel="stylesheet" href="<?=base_url()?>resources/css/base.css">
   <link rel="stylesheet" href="<?=base_url()?>resources/css/vendor.css"> 
   <link rel="stylesheet" href="<?=base_url()?>resources/css/main.css">    
   <link rel="stylesheet" href="<?=base_url()?>resources/css/hero_video.css">    

   <!-- script
   ================================================== -->
	<script src="<?=base_url()?>resources/js/modernizr.js"></script>
	<script src="<?=base_url()?>resources/js/pace.min.js"></script>

   <!-- favicons
	================================================== -->
	<link rel="icon" type="image/png" href="<?=base_url()?>resources/images/favicon.png">

</head>

<body id="top">
	
	<!-- header 
   ================================================== -->
   <header class="main-header">
   	
   	<div class="logo">
	      <a href="index.html">Elevate</a>
	   </div> 

	   <a class="menu-toggle" href="#"><span>Menu</span></a>   	

   </header>

   <!-- main navigation 
   ================================================== -->
   <nav id="menu-nav-wrap">

   	<h3>Navigation</h3>   	
		<ul class="nav-list">
			<li><a class="smoothscroll" href="<?=base_url()?>" title="">Home</a></li>
			<li><a class="smoothscroll" href="<?=base_url()?>visitors/sites" title="">Suitable Sites</a></li>
			<li><a class="smoothscroll" href="<?=base_url()?>visitors/heatmap" title="">SSAM</a></li>
			<li><a class="smoothscroll" href="Water Paramter Map" title="">Water Parameter Map</a></li>

		<h3>Admin</h3>  

		<div class="action">
			<a class="button" href="#">Login Here</a>
		</div>

	</nav> <!-- /menu-nav-wrap -->


	<!-- main content wrap
   ================================================== -->
   <div id="main-content-wrap">