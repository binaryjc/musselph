
    <script src='<?=base_url()?>resources/js/mapbox.js'></script>
    <!--<script src='<?=base_url()?>resources/js/zoom.js'></script>-->
    <link href='<?=base_url()?>resources/css/mapbox.css' rel='stylesheet' />

    <script src='<?=base_url()?>resources/js/mapbox-locate.min.js'></script>
    <link href='<?=base_url()?>resources/css/mapbox-locate.css' rel='stylesheet' /> 
    <!--<link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-locatecontrol/v0.43.0/L.Control.Locate.mapbox.css' rel='stylesheet' />-->
    <!--[if lt IE 9]>
    <link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-locatecontrol/v0.43.0/L.Control.Locate.ie.css' rel='stylesheet' />
    <![endif]-->
    <link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-locatecontrol/v0.43.0/css/font-awesome.min.css' rel='stylesheet' />

    <script src='https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.1.1/ekko-lightbox.min.js'></script>
    <link href='https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.1.1/ekko-lightbox.min.css' rel='stylesheet' />
        <?php /*disable video header ?>
            <div class="homepage-hero-module">
            <div class="video-container">
                <div class="filter"></div>
                <video autoplay loop class="fillWidth">
                    <source src="<?=base_url()?>resources/videos/pebbles.mp4" type="video/mp4" />Your browser does not support the video tag. I suggest you upgrade your browser.
                    <source src="<?=base_url()?>resources/videos/All-set.webm" type="video/webm" />Your browser does not support the video tag. I suggest you upgrade your browser.
                </video>
               <!-- <div class="poster">
                    <img src="<?=base_url()?>resources/images/All-set.jpg" alt="">
                </div>-->
                <div class="title-container">
                    <div class="headline" id="headling_text_container">
                        <!--Original logo height 80px-->
                        <img class="affiliate_image" src="<?=base_url()?>resources/images/pcaarrd.png" alt="DOST-PCAARRD" title="DOST-PCAARRD">
                        <img src="<?=base_url()?>resources/images/mussel_ph_logo_vintage.png">
                        <img class="affiliate_image" src="<?=base_url()?>resources/images/upv.png" alt="UP Visayas" title="University of the Philippines Visayas">
                        <div>
                            <h2 class="text-logo">Mussel Philippines</h2>
                            <h3 class="text-white text-thin">Suitability Assessment and Database Development <br/> for enhanced mussel culture management using geospatial technologies</h3>
                            <h5 class="text-white text-thin"><a class="coverr-nav-item btn-track" href="<?=base_url()?>visitors/about" data-event="MondayScroll_Click" style="text-decoration: none; border-bottom: 1px dotted #fff; color: #fff;">Learn more about the project</a></h5>
                        </div>                
                        
                    </div>

                    <div class="text-center" id="loadBannerVideoSpinner" style="background-color: rgba(0, 0, 0, 0.5); padding: 10px; border-radius: 4px; display: none; margin-top: 20px;">
                        <h5 class="text-thin text-primary">Loading Coverr... <i class="fa fa-circle-o-notch fa-spin"></i></h5>
                    </div>
                </div>
            </div>
            </div>
            <?php */?>

<div class="main-wrap">
    <input id="slide-sidebar" type="checkbox" role="button" />
        <label for="slide-sidebar" id="slide_toggle"><i class="fa fa-bars" aria-hidden="true"></i></label>
    <div class="sidebar">
            <div class="frmSearch">
                <img class="img-responsive img-align-center" src="<?=base_url()."resources/images/"?>logo_small.png">
                <h4>Download Site Maps</h4>
            <div id="suggesstion-box"></div>
                <form method="GET" action="<?=base_url()?>visitors/download_map">
                    <label>Select Site</label>
                    <select class="form-control" name="filt_site" id="filt_site">
                        <optgroup label="Existing">                        
                            <option value="Batan">Batan (Aklan)</option>
                            <option value="Sapian">Sapian (Capiz)</option>
                            <option value="Panay">Pan-ay (Capiz)</option>
                            <option value="Villareal">Villareal (Samar)</option>
                            <option value="Jiabong">Jiabong (Samar)</option>
                            <option value="Tarangnan">Tarangnan (Samar)</option>
                            <option value="Hinigaran">Hinigaran (Negros Occidental)</option>
                            <option value="Abucay">Abucay (Bataan)</option>
                        </optgroup>
                        <optgroup label="Suitable"> 
                            <option value="Calape">Calape (Bohol)</option>
                            <option value="Hagnaya">Hagnaya (Cebu)</option>
                            <option value="Murcielagos">Murcielagos Bay(Zamboanga del Norte and Misamis Occidental)</option>
                            <option value="Panguil">Panguil Bay(Northern Mindanao)</option>
                            <option value="Placer">Canal Bay - Placer (Surigao del Norte)</option>
                            <option value="Mati">Mati (Davao Oriental)</option>
                            <option value="Bislig">Bislig (Surigao del Sur)</option>
                            <option value="Panabo">Panabo (Davao del Norte)</option>
                            <option value="Bais">South Bais Bay (Negros Oriental)</option>
                            <option value="Sagay">Sagay (Negros Occidental)</option>
                            <option value="Balanacan">Balanacan Cove (Marinduque)</option>
                            <option value="Calancan">Calancan Bay (Marinduque)</option>
                            <option value="Salomague">Salomague (Marinduque)</option>
                            <option value="Buguey">Buguey (Cagayan)</option>
                            <option value="Barotac">Barotac Viejo (Iloilo)</option>
                            <option value="Guimaras">Guimaras (Guimaras)</option>
                        </optgroup>
                        <optgroup label="Less Suitable"> 
                            <option value="Talibon">Talibon (Bohol)</option>
                            <option value="Moalboal">Moalboal (Cebu)</option>
                            <option value="Macajalar">Macajalar Bay (Misamis Oriental)</option>
                            <option value="Campomanes">Campomanes Bay (Negros Occidental)</option>
                            <option value="Nabulao">Nabulao Bay (Negros Occidental)</option>
                            <option value="Tayabas">Tayabas Bay (Marinduque)</option>
                            <option value="Lopez">Lopez Bay (Quezon)</option>
                            <option value="Cagayan">Cagayan River (Cagayan)</option>
                        </optgroup>
                        <optgroup label="Suitable But Not Recommended"> 
                            <option value="Candijay">Candijay (Bohol)</option>
                            <option value="Carmen">Carmen (Cebu)</option>
                            <option value="Carrascal">Carrascal Bay (Surigao del Norte)</option>
                            <option value="Anibawan">Anibawan Bay (Quezon)</option>
                            <option value="Patnanungan">Patnanungan (Quezon)</option>
                            <option value="Burdeos">Burdeos Bay (Quezon)</option>
                        </optgroup>
                    </select><br/>
                    <label>Select Parameter</label>
                    <select class="form-control" name="filt_param" id="filt_param">
                        <option value="temp">Temperature</option>
                        <option value="do">Dissolved Oxygen</option>
                        <option value="sal">Salinity</option>
                        <option value="ph">pH</option>
                    </select><br/>
                    <input type="button" id="submit_filter" name="" value="Download" class="form-control"><br/>
                    <div id="download_link" class="hidden">
                        <a href="" id="download_click">Click here if your download has not started</a>
                    </div>
                    <a href="<?=base_url()?>visitors/map_gallery" class="btn btn-default btn-block">Map Gallery</a>
                </form>
            </div>
    </div>
    <div id='map'></div>
</div>

    <div id="suitable_sites_filter_checkbox_container">
        <h5>Filter Sites</h5>
        <label><input type="checkbox" id="existing_site_check" checked><img class="img-responsive" src="<?=base_url()."resources/images/map_icons/"?>church.png"> Existing Sites</label>
        <label><input type="checkbox" id="suitable_site_check" checked><img class="img-responsive" src="<?=base_url()."resources/images/map_icons/"?>suitable.png"> Suitable Sites</label>
        <label><input type="checkbox" id="less_site_check" checked><img class="img-responsive" src="<?=base_url()."resources/images/map_icons/"?>less.png"> Less Suitable Sites</label>  
        <label><input type="checkbox" id="nr_site_check" checked><img class="img-responsive" src="<?=base_url()."resources/images/map_icons/"?>nr.png">Suitable But <br/><span style="margin-left:32px;">Not Recommended</span></label> 
        <label><input type="checkbox" id="red_site_check"><img class="img-responsive" src="<?=base_url()."resources/images/map_icons/"?>red.png"> Red Tide Sites</label>   
        <br/>
        <div class="panel-group" id="accordion">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                Existing Sites</a>
              </h4>
            </div>
            <div id="collapse1" class="panel-collapse collapse in">
              <div class="panel-body">        
                Sites under this category are identified to have existing green mussel culture. These areas are the
                suppliers of green mussel in the country. Physico-chemical and biological parameters are found to be
                ideal and at optimum range. Mussel spats are also abundant in these areas.
                </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                Suitable Sites</a>
              </h4>
            </div>
            <div id="collapse2" class="panel-collapse collapse">
              <div class="panel-body">These areas have been identified to have ideal and optimum physico-chemical and biological
            parameters. There is no known history of mussel culture in these areas and could possibly serve as
            expansion sites.</div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                Less Suitable Sites</a>
              </h4>
            </div>
            <div id="collapse3" class="panel-collapse collapse">
              <div class="panel-body">These sites are identified as enclosed areas but they do not meet the optimum requirements for green mussel growth.</div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
                Suitable but Not Recommended</a>
              </h4>
            </div>
            <div id="collapse4" class="panel-collapse collapse">
              <div class="panel-body">Sites under these category are identified to be suitable but are not recommended due to conditions such as seasonal changes, proper zoning, and site alteration.
              </div>
            </div>
          </div>
        </div> 

    </div>
    <input type="hidden" id="locate_self_lat" value="">
    <input type="hidden" id="locate_self_lng" value="">

<script>
    //CUSTOMMARKER CREATOR
    var MyCustomMarker = L.Marker.extend({
                                bindPopup: function(htmlContent, options) {
                                    if (options && options.showOnMouseOver) {
                                        L.Marker.prototype.bindPopup.apply(this, [htmlContent, options]);
                                        this.off("click", this.openPopup, this);
                                        this.on("mouseover", function(e) {
                                            var target = e.originalEvent.fromElement || e.originalEvent.relatedTarget;
                                            var parent = this._getParent(target, "leaflet-popup");
                                            if (parent == this._popup._container)
                                                return true;
                                            this.openPopup();
                                        }, this);
                                        this.on("mouseout", function(e) {
                                            var target = e.originalEvent.toElement || e.originalEvent.relatedTarget;
                                            if (this._getParent(target, "leaflet-popup")) {
                                                L.DomEvent.on(this._popup._container, "mouseout", this._popupMouseOut, this);
                                                return true;
                                            }
                                            this.closePopup();
                                        }, this);
                                    }
                                },
                                _popupMouseOut: function(e) {
                                    L.DomEvent.off(this._popup, "mouseout", this._popupMouseOut, this);
                                    var target = e.toElement || e.relatedTarget;
                                    if (this._getParent(target, "leaflet-popup"))
                                        return true;
                                    if (target == this._icon)
                                        return true;
                                    this.closePopup();
                                },
                                _getParent: function(element, className) {
                                    if (element.parentNode) {
                                        parent = element.parentNode;
                                    }
                                    while (parent != null) {
                                        if (parent.className && L.DomUtil.hasClass(parent, className))
                                            return parent;
                                        parent = parent.parentNode;
                                    }
                                    return false;
                                }
    });
    //END CUSTOM MARKER

    customMarker = L.Marker.extend({
       options: { 
          category: 'Custom data!',
       }
    });


    //L.mapbox.accessToken = 'pk.eyJ1IjoiYmluYXJ5amMiLCJhIjoiY2lrc3p1Nm5nMDAxeHVnbTM4NmZtYjJkZCJ9.BSH4W1Y1GssKFpB10hjQWw';
    //var map = L.mapbox.map('map', 'mapbox.emerald').setView([10.700713385402713, 122.56364822387697], 15);
    //L.control.locate().addTo(map);

      var map = L.map('map',{ zoomControl:false,scrollWheelZoom:false }).setView([10.722345344678637,122.5605583190918], 7);
      //L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
      L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/outdoors-v9/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoiYmluYXJ5amMiLCJhIjoiY2lrc3p1Nm5nMDAxeHVnbTM4NmZtYjJkZCJ9.BSH4W1Y1GssKFpB10hjQWw', {
          attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a> | <a style="color:green;" href="https://www.facebook.com/binarywebdev/">binaryjc</a>',
          maxZoom: 18,
          id: 'mapbox.outdoors',
          accessToken: 'pk.eyJ1IjoiYmluYXJ5amMiLCJhIjoiY2lrc3p1Nm5nMDAxeHVnbTM4NmZtYjJkZCJ9.BSH4W1Y1GssKFpB10hjQWw'
      }).addTo(map);
    //map.scrollZoom.disable();
    //
    new L.Control.Zoom({ position: 'topright' }).addTo(map);
    //
    /*coordinate setter
    var coordinates = document.getElementById('coordinates');
    var marker2 = L.marker([10.700713385402713, 122.56364822387697], {
        draggable: true
    }).addTo(map);
    marker2.on('dragend', ondragend);
    ondragend();
    function ondragend() {
        var m = marker2.getLatLng();
        coordinates.innerHTML = 'Latitude: ' + m.lat + '<br />Longitude: ' + m.lng;
    }*/

    //ICONS
        var churchIcon = L.icon({
            iconUrl: '<?=base_url()?>resources/images/map_icons/church.png',
            iconSize: [40, 50],
            iconAnchor: [25, 40],
            popupAnchor: [-5, -40],
            className: 'church-icon',
            shadowUrl: '<?=base_url()?>resources/images/map_icons/marker-shadow.png',
            shadowSize:   [40, 40],
            shadowAnchor: [17, 32]
        });
        var suitableIcon = L.icon({
            iconUrl: '<?=base_url()?>resources/images/map_icons/suitable.png',
            iconSize: [40, 50],
            iconAnchor: [25, 40],
            popupAnchor: [-5, -40],
            className: 'church-icon',
            shadowUrl: '<?=base_url()?>resources/images/map_icons/marker-shadow.png',
            shadowSize:   [40, 40],
            shadowAnchor: [17, 32]
        });
        var lessIcon = L.icon({
            iconUrl: '<?=base_url()?>resources/images/map_icons/less.png',
            iconSize: [40, 50],
            iconAnchor: [25, 40],
            popupAnchor: [-5, -40],
            className: 'church-icon',
            shadowUrl: '<?=base_url()?>resources/images/map_icons/marker-shadow.png',
            shadowSize:   [40, 40],
            shadowAnchor: [17, 32]
        });
        var redIcon = L.icon({
            iconUrl: '<?=base_url()?>resources/images/map_icons/red.png',
            iconSize: [40, 50],
            iconAnchor: [25, 40],
            popupAnchor: [-5, -40],
            className: 'church-icon',
            shadowUrl: '<?=base_url()?>resources/images/map_icons/marker-shadow.png',
            shadowSize:   [40, 40],
            shadowAnchor: [17, 32]
        });
        var nrIcon = L.icon({
            iconUrl: '<?=base_url()?>resources/images/map_icons/nr.png',
            iconSize: [40, 50],
            iconAnchor: [25, 40],
            popupAnchor: [-5, -40],
            className: 'church-icon',
            shadowUrl: '<?=base_url()?>resources/images/map_icons/marker-shadow.png',
            shadowSize:   [40, 40],
            shadowAnchor: [17, 32]
        });

    //END ICONS

    //LAYER ARRAY GROUPS - Pre Defined

        var existing_layer_array = [];
        var suitable_layer_array = [];
        var less_layer_array = [];
        var red_layer_array = [];
        var nr_layer_array = [];

    var getAllLocations = function(){
        <?php
        $getAllLocations = base_url()."mapbox/getAllLocations/";
        $getAllRed = base_url()."mapbox/getAllReds/";
        ?>
        console.log("<?= $getAllLocations; ?>");
        $.getJSON("<?= $getAllLocations; ?>", function (data){  
        $.getJSON("<?= $getAllRed; ?>", function (red_data){  
            $.each(data, function(id,entry_data){ 
                console.log(entry_data['name']);
                console.log(entry_data['latlng']);
                console.log(entry_data['content']);
                var iconResulted = entry_data['icon'];
                var coordinatesResulted = entry_data['latlng'];
                var res = coordinatesResulted.split(",");
                var lat = parseFloat(res[0], 10);
                var lng = parseFloat(res[1], 10);
                var latlng = L.latLng(lat, lng);
                var name = entry_data['name'];

                if (iconResulted == "icon.png") {
                    iconChoosen = churchIcon;
                };
                if (iconResulted == "suitable.png") {
                    iconChoosen = suitableIcon;
                };
                if (iconResulted == "less.png") {
                    iconChoosen = lessIcon;
                };
                if (iconResulted == "nr.png") {
                    iconChoosen = nrIcon;
                };

                var banner_link = "<?=base_url()?>resources/images/location_images/"+entry_data['banner'];
                var banner_img = '<img class="pop_banner_image" src="'+banner_link+'">';
                //var content = banner_img+'<br/><br/><h3 class="text-bold">'+entry_data['name']+'</h3>'+entry_data['content']+'<br/><br/>';// disabled content and breaks
                var content = '<h3 class="text-bold pop-title">'+entry_data['name']+'</h3>'+entry_data['content']; 

                //parameters
                var dissolved_oxygen = entry_data['dissolved_oxygen'];
                var dissolved_oxygen_mg = entry_data['dissolved_oxygen_mg'];
                var salinity = entry_data['salinity'];
                var temperature = entry_data['temperature'];
                var chlorophyll_a = entry_data['chlorophyll_a'];
                var reactive_phosphorous = entry_data['reactive_phosphorous'];
                var reactive_nitrate = entry_data['reactive_nitrate'];
                var pom = entry_data['pom'];
                var turbidity = entry_data['turbidity'];
                var ph = entry_data['ph'];
                var fecal_coliform = entry_data['fecal_coliform'];
                var spc = entry_data['spc'];
                var tds = entry_data['tds'];


                //content += '<div class="col-xs-12"><div class="col-xs-6">';
                if (dissolved_oxygen > 0){     
                    content += '<img alt="" class="paramerter_icon" src="<?php echo base_url();?>resources/images/parameter_icons/oxygen-tank.png"/>&nbsp;<span class="text-bold">Dissolved Oxygen:</span>&nbsp;'+dissolved_oxygen+'<br/>';
                }
                if (dissolved_oxygen_mg > 0){     
                    content += '<img alt="" class="paramerter_icon" src="<?php echo base_url();?>resources/images/parameter_icons/oxygen-tank.png"/>&nbsp;<span class="text-bold">Dissolved Oxygen mg/L:</span>&nbsp;'+dissolved_oxygen_mg+'<br/>';
                }
                if (salinity > 0){ 
                    content += '<img alt="" class="paramerter_icon" src="<?php echo base_url();?>resources/images/parameter_icons/salinity.png"/>&nbsp;<span class="text-bold">Salinity:</span>&nbsp;'+salinity+'<br/>';
                }
                if (temperature > 0){ 
                    content += '<img alt="" class="paramerter_icon" src="<?php echo base_url();?>resources/images/parameter_icons/temperature.png"/>&nbsp;<span class="text-bold">temperature:</span>&nbsp;'+temperature+'<br/>';
                }
                if (chlorophyll_a > 0){ 
                    content += '<img alt="" class="paramerter_icon" src="<?php echo base_url();?>resources/images/parameter_icons/chloropyll.png"/>&nbsp;<span class="text-bold">Chlorophyll A:</span>&nbsp;'+chlorophyll_a+'<br/>';
                }
                if (reactive_phosphorous > 0){ 
                    content += '<img alt="" class="paramerter_icon" src="<?php echo base_url();?>resources/images/parameter_icons/phosphorous.png"/>&nbsp;<span class="text-bold">Reactive Phosphorous:</span>&nbsp;'+reactive_phosphorous+'<br/>';
                }
                if (reactive_nitrate > 0){ 
                    content += '<img alt="" class="paramerter_icon" src="<?php echo base_url();?>resources/images/parameter_icons/nitrate.png"/>&nbsp;<span class="text-bold">Reactive Nitrate:</span>&nbsp;'+reactive_nitrate+'<br/>';
                }
                if (pom > 0){                
                    content += '<img alt="" class="paramerter_icon" src="<?php echo base_url();?>resources/images/parameter_icons/pom.png"/>&nbsp;<span class="text-bold">Particulate Organic Matter:</span>&nbsp;'+pom+'<br/>';
                }
                if (turbidity > 0){
                    content += '<img alt="" class="paramerter_icon" src="<?php echo base_url();?>resources/images/parameter_icons/turbidity.png"/>&nbsp;<span class="text-bold">Turbidity:</span>&nbsp;'+turbidity+'<br/>';
                }
                if (ph > 0){
                    content += '<img alt="" class="paramerter_icon" src="<?php echo base_url();?>resources/images/parameter_icons/ph.png"/>&nbsp;<span class="text-bold">pH:</span>&nbsp;'+ph+'<br/>';
                }
                if (fecal_coliform > 0){
                    content += '<img alt="" class="paramerter_icon" src="<?php echo base_url();?>resources/images/parameter_icons/fecal.png"/>&nbsp;<span class="text-bold">Fecal Coliform:</span>&nbsp;'+fecal_coliform+'<br/>';
                }
                if (spc > 0){
                    content += '<img alt="" class="paramerter_icon" src="<?php echo base_url();?>resources/images/parameter_icons/spc.png"/>&nbsp;<span class="text-bold">Specific Conductance:</span>&nbsp;'+spc+'<br/>';
                }
                if (tds > 0){
                    content += '<img alt="" class="paramerter_icon" src="<?php echo base_url();?>resources/images/parameter_icons/melt.png"/>&nbsp;<span class="text-bold">Total Dissolved Solids:</span>&nbsp;'+tds+'<br/>';
                }
               // content += '</div></div>';
               // 
                //map galleyr
                if (entry_data['map_images_path'] != ''){
                        var map_gallery = 
                    '<div class="col-xs-12 float-none no-padding">'+
                    '<a href="<?=base_url()?>resources/images/sites/'+entry_data['map_images_path']+'/do.jpg" data-toggle="lightbox" data-gallery="'+entry_data['map_images_path']+'-gallery" class="col-sm-6">'+
                        '<img src="<?=base_url()?>resources/images/sites/'+entry_data['map_images_path']+'/thumbnails/do_tn.jpg" class="img-responsive">'+
                    '</a>'+
                    '<a href="<?=base_url()?>resources/images/sites/'+entry_data['map_images_path']+'/ph.jpg" data-toggle="lightbox" data-gallery="'+entry_data['map_images_path']+'-gallery" class="col-sm-6">'+
                        '<img src="<?=base_url()?>resources/images/sites/'+entry_data['map_images_path']+'/thumbnails/ph_tn.jpg" class="img-responsive">'+
                    '</a>'+
                    '<a href="<?=base_url()?>resources/images/sites/'+entry_data['map_images_path']+'/sal.jpg" data-toggle="lightbox" data-gallery="'+entry_data['map_images_path']+'-gallery" class="col-sm-6">'+
                        '<img src="<?=base_url()?>resources/images/sites/'+entry_data['map_images_path']+'/thumbnails/sal_tn.jpg" class="img-responsive">'+
                    '</a>'+
                    '<a href="<?=base_url()?>resources/images/sites/'+entry_data['map_images_path']+'/temp.jpg" data-toggle="lightbox" data-gallery="'+entry_data['map_images_path']+'-gallery" class="col-sm-6">'+
                        '<img src="<?=base_url()?>resources/images/sites/'+entry_data['map_images_path']+'/thumbnails/temp_tn.jpg" class="img-responsive">'+
                    '</a>'+
                    '<small>Click the thumbnail to enlarge</small>'+
                    '</div>';
                    content += map_gallery;
                }

                var marky = L.marker(latlng, {icon: iconChoosen}).addTo(map).bindPopup(content);

                if (entry_data['category'] == 'existing' ) {
                    existing_layer_array.push(marky);
                }else if (entry_data['category'] == 'suitable' ){
                    suitable_layer_array.push(marky);
                }else if (entry_data['category'] == 'nr' ){
                    nr_layer_array.push(marky);
                }else{
                    less_layer_array.push(marky);
                }

            });

            $.each(red_data, function(id,entry_data2){ 

                //var lat = parseFloat(res[0], 10);
                //var lng = parseFloat(res[1], 10);
                var latlng = L.latLng(entry_data2['lat'], entry_data2['lng']);
                var name = entry_data2['location'];
            
                var content = '<h3 class="text-bold pop-title">'+entry_data2['location']+'</h3>'; // disabled content and breaks

                //parameters
                var red_tide = entry_data2['red_sum'];
                if (red_tide > 0){
                    content += '<img alt="" class="paramerter_icon" src="<?php echo base_url();?>resources/images/parameter_icons/red_tide.png"/>&nbsp;<span class="text-bold">Red Tide Frequency:</span>&nbsp;'+red_tide+'<br/>';
                }

                var marky = L.marker(latlng, {icon: redIcon}).bindPopup(content);
                
                red_layer_array.push(marky);
                

            });
            
            //CREATE LAYER GROUPS NOW
                var existing_layer = L.layerGroup(existing_layer_array).addTo(map);
                var suitable_layer = L.layerGroup(suitable_layer_array).addTo(map);
                var less_layer = L.layerGroup(less_layer_array).addTo(map);
                //var red_layer = L.layerGroup(red_layer_array).addTo(map);
                var red_layer = L.layerGroup(red_layer_array); //not default added to map
                var nr_layer = L.layerGroup(nr_layer_array); //not default added to map

            //LAYER GROUP FILTER BUTTON ACTIONS
                document.getElementById("existing_site_check").onclick = function() {
                    if (this.checked) { map.addLayer(existing_layer);} else {map.removeLayer(existing_layer);}
                }
                document.getElementById("suitable_site_check").onclick = function() {
                    if (this.checked) { map.addLayer(suitable_layer);} else {map.removeLayer(suitable_layer);}
                }
                document.getElementById("less_site_check").onclick = function() {
                    if (this.checked) { map.addLayer(less_layer);} else {map.removeLayer(less_layer);}
                }
                document.getElementById("red_site_check").onclick = function() {
                    if (this.checked) { map.addLayer(red_layer);} else {map.removeLayer(red_layer);}
                }
                document.getElementById("nr_site_check").onclick = function() {
                    if (this.checked) { map.addLayer(nr_layer);} else {map.removeLayer(nr_layer);}
                }
            
        });
        });
    }



    var getLocationByFilter = function(category,string_search){
        <?php
        $getLocationByFilter = base_url()."mapbox/getLocationByFilter/";
        ?>

        //INSERT CLEAR MAP COMMAND
        
        $.getJSON("<?= $getLocationByFilter; ?>"+category+"/"+string_search, function (data){  
            $.each(data, function(id,entry_data){ 
            console.log(entry_data['name']);
            console.log(entry_data['latlng']);
            console.log(entry_data['content']);
            var iconResulted = entry_data['icon'];
            var coordinatesResulted = entry_data['latlng'];
            var res = coordinatesResulted.split(",");
            var lat = parseFloat(res[0], 10);
            var lng = parseFloat(res[1], 10);
            var latlng = L.latLng(lat, lng);
            var content = entry_data['content'];
            var name = entry_data['name'];

            if (iconResulted == "church.png") {
                iconChoosen = churchIcon;
            };
            if (iconResulted == "bus.png") {
                iconChoosen = terminalIcon;
            };
            if (iconResulted == "school.png") {
                iconChoosen = schoolIcon;
            };
            if (iconResulted == "hospital.png") {
                iconChoosen = hospitalIcon;
            };

               var marky = new MyCustomMarker(latlng, {
                    icon: iconChoosen
                }).addTo(map);
                marky.bindPopup(content, {
                    showOnMouseOver: true
                });

            });
            
        });

    }

    $(document).ready(function(){
        getAllLocations();
    });


    $("#submit_filter").click(function(){
        $("#download_link").show();
        var site = $('#filt_site').val();
        var param = $('#filt_param').val();
        var url = '<?=base_url()?>visitors/download_map/'+site+'/'+param;
        window.open(url, '_blank');

    });

    /*SEARCHING
    $( "#search_query" ).keyup(function(e) {

    if($( "#search_query").val() == 0){
       $( "#search_results" ).empty();
    }

    if(e.which == 13) {
       $( "#search_results" ).empty();
       searchLocation($( "#search_query").val());
    }

    });

    function searchLocation(search_text){
        <?php
        $searchLocation = base_url()."mapbox/searchLocation/";
        ?>
        console.log("<?= $searchLocation; ?>"+search_text);
        $.getJSON("<?= $searchLocation; ?>"+search_text, function (data){  
            $( "#search_results").empty();
            $.each(data, function(id,entry_data){
                var coordinatesResulted = entry_data['latlng'];
                var res = coordinatesResulted.split(",");
                var lat = parseFloat(res[0], 10);
                var lng = parseFloat(res[1], 10);
                var latlng = L.latLng(lat, lng);
                $("#search_results").append(
                    //function out to add effect
                    '<li class="search-result-entry"><a href="#" onclick="map.panTo(['+res+']);">'+entry_data['name']+'</a></li>'
                    );
            });
        });
    }*/


</script>

<script>
    /* hero video
        
        //jQuery is required to run this code
        $( document ).ready(function() {

            scaleVideoContainer();

            initBannerVideoSize('.video-container .poster img');
            initBannerVideoSize('.video-container .filter');
            initBannerVideoSize('.video-container video');

            $(window).on('resize', function() {
                scaleVideoContainer();
                scaleBannerVideoSize('.video-container .poster img');
                scaleBannerVideoSize('.video-container .filter');
                scaleBannerVideoSize('.video-container video');
            });

        });

        function scaleVideoContainer() {

            var height = $(window).height() + 5;
            var unitHeight = parseInt(height) + 'px';
            $('.homepage-hero-module').css('height',unitHeight);

        }

        function initBannerVideoSize(element){

            $(element).each(function(){
                $(this).data('height', $(this).height());
                $(this).data('width', $(this).width());
            });

            scaleBannerVideoSize(element);

        }

        function scaleBannerVideoSize(element){

            var windowWidth = $(window).width(),
            windowHeight = $(window).height() + 5,
            videoWidth,
            videoHeight;

            console.log(windowHeight);

            $(element).each(function(){
                var videoAspectRatio = $(this).data('height')/$(this).data('width');

                $(this).width(windowWidth);

                if(windowWidth < 1000){
                    videoHeight = windowHeight;
                    videoWidth = videoHeight / videoAspectRatio;
                    $(this).css({'margin-top' : 0, 'margin-left' : -(videoWidth - windowWidth) / 2 + 'px'});

                    $(this).width(videoWidth).height(videoHeight);
                }

                $('.homepage-hero-module .video-container video').addClass('fadeIn animated');

            });
        }
    */
</script>

<script type="text/javascript">

(function($) {          
    $(document).ready(function(){                    
        $(window).scroll(function(){                          
            if ($(this).scrollTop() > 200) {
                $('header').fadeIn(500);
            } else {
                $('header').fadeOut(500);
            }
        });
    });
})(jQuery);

$(document).on('click', '[data-toggle="lightbox"]', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
});
</script>
