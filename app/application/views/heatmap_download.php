    <script src="https://unpkg.com/leaflet@1.0.3/dist/leaflet.js"></script>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.3/dist/leaflet.css" />
    <script src='<?=base_url()?>resources/js/bundle.min.js'></script>
    <style>
        html {background: #222222; padding: 40px 200px;font-family: 'Lato', sans-serif;}
        body{background-color:#222222;}
        h2,p {color: white;}
        .manualButton {
            padding:15px;
            background-color: #F0CA4D;
            margin-left: 10px;
            text-decoration: none;
            font-weight: 900;
            color: white;
            text-transform:uppercase;
            transition: background-color .5s;
            border: none;
            border-radius: 2px;
            cursor: pointer;
        }

    </style>

    <div class="col-xs-12">
        <h2>Welcome to your downloadable map <button class="manualButton hidden" id="dl_btn" onclick="manualPrint()">Download</button> <span class="loader-wrapper"></span></h2>
        <div id='map' class="" style="height:800px; width:100%"></div>
    </div>

    <script type="text/javascript">

        //http://localhost/musselph/app/visitors/heatmap_download?lat=10.722345344678637&lang=122.5605583190918&year=2015&month=12
        var map = L.map('map',{ zoomControl:false }).setView([<?=$_GET['lat']?>,<?=$_GET['long']?>], 9);

        L.TileLayer.ShowAsDiv = L.TileLayer.extend({
          createTile: function (coords, done) {
              var tile = document.createElement('div');

              if (this.options.crossOrigin) {
                tile.crossOrigin = '';
              }

              tile.style.backgroundImage = "url('"+this.getTileUrl(coords)+"')";
              tile.style.visibility = 'inherit';

              return tile;
          },
        });

        L.tileLayer.showAsDiv = function(args) {
            return new L.TileLayer.ShowAsDiv(args);
        };

        var tiles = L.tileLayer.showAsDiv( 'http://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}.png', {
            maxZoom: 18,
            attribution: 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors',
        }).addTo(map);

        map.dragging.disable();
        map.touchZoom.disable();
        map.doubleClickZoom.disable();
        map.scrollWheelZoom.disable();
        map.boxZoom.disable();
        map.keyboard.disable();
        /*
        var tiles = L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}.png', {
            maxZoom: 18,
            attribution: 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors',
        }).addTo(map);*/

        var printer = L.easyPrint({
            tileLayer: tiles,
            sizeModes: ['Current'],
            filename: 'myMap',
            exportOnly: true,
            hideControlContainer: true
        }).addTo(map);

        function manualPrint () {
            spinnerLoad();
            printer.printMap('CurrentSize', 'MyDownloadedMusselMap');
        }

    //SETTINGS

        // get color depending on population density value
        function getColor(d) { //chlor_a - > yellow -> green dapat for chlor //http://www.december.com/html/spec/colorhslhex10.html
            return d > 12 ? '#08601b' :
                    d > 1  ? '#20ce45' :
                    d > 0  ? '#99f1a8' :
                                '#00000000';
        }
        function getColor_sst(d) { //sst - blue -> red dapat temperature
            return d > 27 ? '#d40b06' :
                    d > 22  ? '#fe6e6e' :
                    d < 22  ? '#febdb8' :
                                '#00000000';
        }

        function getColor_sal(d) { //salinity blue - upcoming data
            return d < 32 ? '#1e03bd' :
                    d > 27 ? '#1e03bd' :
                    d > 22  ? '#3480f0' :
                    d < 22  ? '#b2d3fc' :
                    d < 0  ? '#00000000' :
                                '#00000000';
        }



        //Blue sa salinity, r d sa temp kag green sa chloro guro pre

        function style(feature) {
            return {
                weight: 2,
                opacity: 1,
                color: 'transparent',
                dashArray: 0,
                fillOpacity: 0.5,
                fillColor: getColor(feature.properties.density)
            };
        }

        function style_sst(feature) {
            return {
                weight: 2,
                opacity: 1,
                color: 'transparent',
                dashArray: 0,
                fillOpacity: 0.5,
                fillColor: getColor_sst(feature.properties.sst)
            };
        }

        function style_sal(feature) {
            return {
                weight: 2,
                opacity: 1,
                color: 'transparent',
                dashArray: 0,
                fillOpacity: 0.5,
                fillColor: getColor_sal(feature.properties.salinity)
            };
        }

        function highlightFeature(e) {
            var layer = e.target;

            layer.setStyle({
                weight: 5,
                //color: '#666',
                dashArray: '',
                fillOpacity: 0.5
            });
            /*
            if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
                layer.bringToFront();
            }*/

            //info.update(layer.feature.properties);
        }

        var geojson;

        function resetHighlight(e) {
            //geojson.resetStyle(e.target);
            //info.update();
        }

        function zoomToFeature(e) {
            map.fitBounds(e.target.getBounds());
        }

        function onEachFeature(feature, layer) {
            layer.on({
                mouseover: highlightFeature,
                mouseout: resetHighlight,
                //click: zoomToFeature
            });
        }

        var get_chlora_data_with_location = function(current_lat,current_lng,withinkmradius,year,month){
            console.log('<?=base_url()?>admins/get_chlora_data_with_location/'+current_lat+'/'+current_lng+'/'+withinkmradius+'/'+year+'/'+month)
            $.getJSON('<?=base_url()?>admins/get_chlora_data_with_location/'+current_lat+'/'+current_lng+'/'+withinkmradius+'/'+year+'/'+month, function(statesData) {
                //console.log(statesData);
                //data is the JSON string

                geojson = L.geoJson(statesData, {
                    style: style,
                    onEachFeature: onEachFeature
                }).addTo(map);


            });
        }

        var get_sst_data_with_location = function(current_lat,current_lng,withinkmradius,year,month){
            console.log('<?=base_url()?>admins/get_sst_data_with_location/'+current_lat+'/'+current_lng+'/'+withinkmradius+'/'+year+'/'+month)
            $.getJSON('<?=base_url()?>admins/get_sst_data_with_location/'+current_lat+'/'+current_lng+'/'+withinkmradius+'/'+year+'/'+month, function(statesData2) {
                //console.log(statesData2);
                //data is the JSON string

                geojson2 = L.geoJson(statesData2, {
                    style: style_sst,
                    onEachFeature: onEachFeature
                }).addTo(map);


            });
        }

        var get_sal_data_with_location = function(current_lat,current_lng,withinkmradius,year,month){
            console.log('<?=base_url()?>admins/get_sal_data_with_location/'+current_lat+'/'+current_lng+'/'+withinkmradius+'/'+year+'/'+month)
            $.getJSON('<?=base_url()?>admins/get_sal_data_with_location/'+current_lat+'/'+current_lng+'/'+withinkmradius+'/'+year+'/'+month, function(statesData3) {
                //console.log(statesData2);
                //data is the JSON string

                geojson3 = L.geoJson(statesData3, {
                    style: style_sal,
                    onEachFeature: onEachFeature
                }).addTo(map);


            });
        }

    //SETTINGS END

            current_position = L.marker([<?=$_GET['lat']?>,<?=$_GET['long']?>],{draggable: true}).addTo(map);
            current_position.on('dragend', ondragend);

            var m = current_position.getLatLng();
            //$('#locate_self_lat').val(m.lat);
            //$('#locate_self_lng').val(m.lng);

            var year = <?=$_GET['year']?>;
            var month = '<?=$_GET['month']?>';

            //if ($('#filt_chl').prop('checked')){
                get_chlora_data_with_location(m.lat,m.lng,100,year,month);
            //}
            //if ($('#filt_temp').prop('checked')){
                setTimeout(function(){
                    get_sst_data_with_location(m.lat,m.lng,100,year,month);
                },1000); //delay for a second so that color is overlayed

                setTimeout(function(){
                    get_sal_data_with_location(m.lat,m.lng,100,year,month);
                },1000); //delay for a second so that color is overlayed
            //}
                setTimeout(function(){
                    $('#dl_btn').toggleClass('hidden');
                },3000); //delay for a second so that color is overlayed


            var spinnerLoad = function(){
              $('.loader-wrapper').text("");
              $('.loader-wrapper').append(
                    '<img src="<?=base_url()?>resources/images/loader_small_blank.gif">'
                );
              $('.loader-wrapper').fadeIn(300);
                $('.loader-wrapper').fadeOut(5000, function() {$(this).empty().show();});
            };
    </script>


