-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 09, 2018 at 04:39 AM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.0.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u984843612_mph`
--

-- --------------------------------------------------------

--
-- Table structure for table `area`
--

CREATE TABLE `area` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `map_images_path` text NOT NULL,
  `banner` varchar(255) NOT NULL DEFAULT 'roxas_mussels.jpg',
  `latlng` text NOT NULL,
  `icon` varchar(30) NOT NULL DEFAULT 'icon.png',
  `category` varchar(350) NOT NULL,
  `content` text NOT NULL,
  `province_id` int(11) NOT NULL DEFAULT '0',
  `temperature` float NOT NULL,
  `dissolved_oxygen_mg` float NOT NULL,
  `salinity` float NOT NULL,
  `ph` float NOT NULL,
  `dissolved_oxygen` float NOT NULL,
  `chlorophyll_a` float NOT NULL,
  `reactive_phosphorous` float NOT NULL,
  `reactive_nitrate` float NOT NULL,
  `pom` float NOT NULL,
  `turbidity` float NOT NULL,
  `fecal_coliform` float NOT NULL,
  `spc` float NOT NULL,
  `tds` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `area`
--

INSERT INTO `area` (`id`, `name`, `map_images_path`, `banner`, `latlng`, `icon`, `category`, `content`, `province_id`, `temperature`, `dissolved_oxygen_mg`, `salinity`, `ph`, `dissolved_oxygen`, `chlorophyll_a`, `reactive_phosphorous`, `reactive_nitrate`, `pom`, `turbidity`, `fecal_coliform`, `spc`, `tds`) VALUES
(1, 'Batan', 'Batan', 'roxas_mussels.jpg', '11.562316,122.460486', 'icon.png', 'existing', '', 0, 28.93, 4.46, 27.14, 7.74, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 'Sapian', 'Sapian', 'roxas_mussels.jpg', '11.528459,122.612499', 'icon.png', 'existing', '', 0, 28.56, 5.11, 32.12, 7.89, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(3, 'Pan-ay', 'Panay', 'roxas_mussels.jpg', '11.590108,122.826336', 'icon.png', 'existing', '', 0, 29.12, 5.39, 20.33, 7.7, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 'Villareal', 'Villareal', 'roxas_mussels.jpg', '11.580941,124.896706', 'icon.png', 'existing', '', 0, 30.04, 7.98, 32.96, 7.84, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 'Jiabong', 'Jiabong', 'roxas_mussels.jpg', '11.738109,124.990302', 'icon.png', 'existing', '', 0, 29.89, 5.07, 32.6, 7.85, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(6, 'Tarangnan', 'Tarangnan', 'roxas_mussels.jpg', '11.905175,124.784338', 'icon.png', 'existing', '', 0, 30, 3.51, 31.89, 7.7, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(7, 'Hinigaran', 'Hinigaran', 'roxas_mussels.jpg', '10.287883,122.872283', 'icon.png', 'existing', '', 0, 30.67, 5.03, 31.43, 7.32, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(8, 'Abucay', 'Abucay', 'roxas_mussels.jpg', '14.728358,120.563894', 'icon.png', 'existing', '', 0, 30.41, 7.03, 20.74, 8.01, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(9, 'Calape', 'Calape', 'roxas_mussels.jpg', '9.895053,123.853158', 'suitable.png', 'suitable', '', 0, 26.98, 5.3, 33.83, 8.18, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(10, 'Hagnaya', 'Hagnaya', 'roxas_mussels.jpg', '11.087409,123.960973', 'suitable.png', 'suitable', '', 0, 28.37, 5.03, 33.11, 8.1, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(11, 'Murcielagos Bay', 'Murcielagos', 'roxas_mussels.jpg', '8.648463,123.540773', 'suitable.png', 'suitable', '', 0, 30.47, 6.16, 33.52, 7.94, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(12, 'Panguil Bay', 'Panguil', 'roxas_mussels.jpg', '8.053903,123.77251', 'suitable.png', 'suitable', '', 0, 30.89, 5.84, 24.07, 7.9, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(13, 'Canal Bay (Placer)', 'Placer', 'roxas_mussels.jpg', '9.696523,125.624139', 'suitable.png', 'suitable', '', 0, 29.18, 6.24, 32.75, 7.97, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(14, 'Mati', 'Mati', 'roxas_mussels.jpg', '6.894712,126.165987', 'suitable.png', 'suitable', '', 0, 31.53, 5.43, 33.8, 7.98, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(15, 'Bislig', 'Bislig', 'roxas_mussels.jpg', '8.209766,126.298184', 'suitable.png', 'suitable', '', 0, 30.35, 5.61, 23.35, 7.63, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(16, 'Panabo', 'Panabo', 'roxas_mussels.jpg', '7.285368,125.701108', 'suitable.png', 'suitable', '', 0, 30.08, 5.8, 31.43, 7.93, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(17, 'South Bais Bay', 'Bais', 'roxas_mussels.jpg', '9.567528,123.14165', 'suitable.png', 'suitable', '', 0, 29.93, 4.83, 33.12, 7.12, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(18, 'Sagay', 'Sagay', 'roxas_mussels.jpg', '10.909169,123.511346', 'suitable.png', 'suitable', '', 0, 30.06, 4.88, 33.16, 6.69, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(19, 'Balanacan Cove', 'Balanacan', 'roxas_mussels.jpg', '13.538578,121.868202', 'suitable.png', 'suitable', '', 0, 31.25, 5.38, 33.73, 7.52, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(20, 'Calancan Bay', 'Calancan', 'roxas_mussels.jpg', '13.497296,122.062322', 'suitable.png', 'suitable', '', 0, 30.33, 5.85, 33.51, 7.72, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(21, 'Salomague', 'Salomague', 'roxas_mussels.jpg', '13.417474,122.115294', 'suitable.png', 'suitable', '', 0, 30.45, 5.11, 33.62, 7.68, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(22, 'Buguey', 'Buguey', 'roxas_mussels.jpg', '18.271279,121.892982', 'suitable.png', 'suitable', '', 0, 32.4, 6.34, 20.77, 7.97, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(23, 'Barotac Viejo', 'Barotac', 'roxas_mussels.jpg', '11.05558,122.92687', 'suitable.png', 'suitable', '', 0, 28.37, 5.68, 34.08, 8.138, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(24, 'Guimaras', 'Guimaras', 'roxas_mussels.jpg', '10.543588,122.527314', 'suitable.png', 'suitable', '', 0, 30.59, 5.27, 33.62, 7.91, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(25, 'Candijay', 'Candijay', 'roxas_mussels.jpg', '9.82448,124.564305', 'less.png', 'less', '', 0, 27.25, 5.73, 32.91, 8.15, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(26, 'Talibon', 'Talibon', 'roxas_mussels.jpg', '10.173376,124.244616', 'less.png', 'less', '', 0, 27.08, 5.62, 34, 8.22, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(27, 'Carmen', 'Carmen', 'roxas_mussels.jpg', '10.586823,124.024923', 'less.png', 'less', '', 0, 29, 5.43, 33.88, 8.2, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(28, 'Moalboal', 'Moalboal', 'roxas_mussels.jpg', '9.979563,123.394933', 'less.png', 'less', '', 0, 27.16, 6.58, 33.33, 8.09, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(29, 'Macajalar Bay', 'Macajalar', 'roxas_mussels.jpg', '8.587976,124.666416', 'less.png', 'less', '', 0, 30.55, 6.22, 32.24, 7.98, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(30, 'Carrascal Bay', 'Carrascal', 'roxas_mussels.jpg', '9.393385,125.953198', 'less.png', 'less', '', 0, 30.42, 5.75, 32.39, 7.91, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(31, 'Campomanes Bay', 'Campomanes', 'roxas_mussels.jpg', '9.69475,122.411237', 'less.png', 'less', '', 0, 29.89, 5.67, 31.33, 7.35, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(32, 'Nabulao Bay', 'Nabulao', 'roxas_mussels.jpg', '9.660661,122.430536', 'less.png', 'less', '', 0, 30.14, 5.4, 33.07, 7.77, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(33, 'Tayabas Bay', 'Tayabas', 'roxas_mussels.jpg', '13.536875,121.952364', 'less.png', 'less', '', 0, 31.34, 5.03, 33.64, 7.59, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(34, 'Anibawan Bay', 'Anibawan', 'roxas_mussels.jpg', '14.976711,122.025411', 'less.png', 'less', '', 0, 29.41, 6.04, 29.66, 7.85, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(35, 'Patnanungan', 'Patnanungan', 'roxas_mussels.jpg', '14.858413,122.106825', 'less.png', 'less', '', 0, 29.82, 6.47, 33.5, 7.81, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(36, 'Burdeos Bay', 'Burdeos', 'roxas_mussels.jpg', '14.866597,121.992909', 'less.png', 'less', '', 0, 29.89, 5.85, 33.51, 7.71, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(37, 'Lopez Bay', 'Lopez', 'roxas_mussels.jpg', '13.92908,122.208177', 'less.png', 'less', '', 0, 30.87, 4.89, 33.66, 7.67, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(38, 'Cagayan River', 'Appari', 'roxas_mussels.jpg', '18.321631,121.643804', 'less.png', 'less', '', 0, 31.33, 6.17, 5.61, 7.85, 0, 0, 0, 0, 0, 0, 0, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `area`
--
ALTER TABLE `area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
