-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 09, 2018 at 01:48 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `musselph`
--

-- --------------------------------------------------------

--
-- Table structure for table `red_tides`
--

CREATE TABLE IF NOT EXISTS `red_tides` (
  `id` int(11) NOT NULL,
  `location` varchar(350) NOT NULL,
  `red_sum` float NOT NULL,
  `lat` varchar(350) NOT NULL,
  `lng` varchar(350) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `red_tides`
--

INSERT INTO `red_tides` (`id`, `location`, `red_sum`, `lat`, `lng`) VALUES
(1, 'Altavas', 10, '11.552022', '122.4612'),
(2, 'Batan', 10, '11.566815', '122.460216'),
(3, 'Camansi', 2, '11.726297', '122.361824'),
(4, 'Mambuquiao', 2, '11.554771', '122.569546'),
(5, 'New Washington', 10, '11.642363', '122.41357'),
(6, 'Abucay', 27, '14.715011', '120.55303'),
(7, 'Balanga', 27, '14.691924', '120.563569'),
(8, 'Limay', 27, '14.525021', '120.609714'),
(9, 'Mariveles', 27, '14.443457', '120.444271'),
(10, 'Orani', 27, '14.599123', '120.338133'),
(11, 'Orion', 27, '14.596388', '120.5893'),
(12, 'Pilar', 27, '14.61474', '120.586509'),
(13, 'Samal', 27, '14.772113', '120.551759'),
(14, 'Biliran Province', 33, '11.535419', '124.410692'),
(15, 'Dauis Bay', 39, '9.647317', '123.837158'),
(16, 'Tagbilaran', 11, '9.657864', '123.843799'),
(17, 'Ivisan', 2, '11.535418', '122.682095'),
(18, 'Pilar', 19, '11.496447', '122.999204'),
(19, 'Pres. Roxas', 2, '11.452071', '122.908715'),
(20, 'Sapian', 14, '11.514081', '122.584688'),
(21, 'Balete Bay', 46, '6.87203', '126.230497'),
(22, 'Matarinao', 88, '11.211371', '125.561445'),
(23, 'Gigantes Island', 25, '11.597462', '123.34344'),
(24, 'Carigara', 49, '11.303195', '124.673832'),
(25, 'Calubian', 7, '11.502806', '124.381598'),
(26, 'Leyte', 33, '11.11116', '125.032967'),
(27, 'Tacloban', 7, '11.258652', '124.980144'),
(28, 'Mandaon', 10, '12.29341', '123.236626'),
(29, 'Milagros', 33, '12.215247', '123.504442'),
(30, 'Honda Bay', 9, '9.956909', '118.879273'),
(31, 'Puerto Princesa Bay', 2, '9.940296', '118.794304'),
(32, 'Alaminos', 5, '16.174612', '120.017217'),
(33, 'Anda', 14, '16.27318', '119.951916'),
(34, 'Bani', 6, '16.238582', '119.902819'),
(35, 'Bolinao', 14, '16.321633', '119.926595'),
(36, 'Wawa', 6, '15.763462', '120.442195'),
(37, 'Daram Island', 26, '11.682449', '124.759811'),
(38, 'Juag Lagoon', 4, '12.552675', '124.110994'),
(39, 'Sorsogon Bay', 25, '12.967203', '124.011427'),
(40, 'Bislig Bay', 25, '8.205136', '126.348959'),
(41, 'Calbayog City', 6, '12.062246', '124.588289'),
(42, 'Cambatutay', 62, '11.892164', '124.785854'),
(43, 'Irong-Irong', 65, '11.911703', '124.785213'),
(44, 'Maqueda', 15, '11.739426', '124.984639'),
(45, 'Villareal', 15, '11.540362', '124.88175'),
(46, 'Zambales (Masinloc Bay)', 16, '15.510263', '119.937743'),
(47, 'Dumanguillas Bay', 68, '8.649447', '123.398977'),
(48, 'Murcielagos Bay', 66, '8.641344', '123.503504'),
(49, 'Misamis Occidental', 66, '8.278482', '123.866981');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `red_tides`
--
ALTER TABLE `red_tides`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `red_tides`
--
ALTER TABLE `red_tides`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=50;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
